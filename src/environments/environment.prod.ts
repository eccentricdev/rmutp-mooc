export const environment = {
  production: false,
  baseUrl: 'https://rmutp.71dev.com/rmutp-mooc-api',
  // rsuAppApi:'https://rsu-app-api.71dev.com/uat/',
  // apiRegister:'https://rsu-reg-api.71dev.com/v2',
  // apiversion:'v1',
  // apiCommonApi: 'https://rsu-common-api.71dev.com/api',
  // openIdApi: 'https://iam71-api.71dev.com/openid',
  // eduApi:'https://education-api.71dev.com/dev/api/',
  clientSettings: {
    authority: 'https://rmutr-iam.71dev.com/',
    client_id: 'rmutr-admin-web-client-prod',
    redirect_uri: 'https://rmutp.71dev.com/rmutp-admin/auth-callback',
    post_logout_redirect_uri: 'https://rmutp.71dev.com/',
    response_type:"token",
    scope:"rmutpadmin",
    filterProtocolClaims: true,
    loadUserInfo: true
  }
  };
  