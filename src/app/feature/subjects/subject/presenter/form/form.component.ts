import { LanguageService } from '../../../../../core/service/subject/language.service';
import { SkillService } from '../../../../../core/service/subject/skill.service';
import { UploadImgService } from '../../../../../core/service/upload/upload-img.service';
import { Component, OnInit } from '@angular/core';
import { BaseTiny } from 'app/core/base/tinymce';
import { SubjectService } from 'app/core/service/subject/subject.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseTiny implements OnInit {

  lang
  skill
  datainternal = {
    subject_code:null,
    subject_name_th:null,
    faculty_name:null,
    instructor_name:null,
    language_uid:null,
    skill_uid:null,
    is_certificate_reward:null,
    status_id:null,
    subject_description:null,
    subject_purpose:null
    
  }
  constructor(
    public uploadSV: UploadImgService,
    private subjectSV:SubjectService,
    private skillSV:SkillService,
    private languageSV:LanguageService,
    private router:Router,

  ) {
    super(uploadSV);
  }

  ngOnInit(): void {
    this.languageSV.getAll().subscribe((x)=>{
      console.log(x)
      this.lang = x
    })
    this.skillSV.getAll().subscribe((x)=>{
      console.log(x)
      this.skill = x
    })
  }

  clear(){
    this.router.navigate(['/app/subject'])
  }

  save(){
    this.subjectSV.add(this.datainternal).subscribe((x)=>{
      console.log(x)
      Swal.fire({
        icon: 'success',
        text: 'บันทึกข้อมูลเรียบร้อย',
        confirmButtonText: `ตกลง`,
      }).then((result) => {
        if (result.isConfirmed) {
          this.router.navigate(['/app/subject'])
        }
      })
    })
  }

  displaylang(id) {
    console.log(id)
    if(id) return this.lang.find(lang => lang.language_uid === id)?.language_name_th
  }

  displayskill(id) {
    console.log(id)
    if(id) return this.skill.find(skill => skill.skill_uid === id)?.skill_name_th
  }
}
