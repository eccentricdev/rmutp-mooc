import { UploadImgService } from './../../../../../../core/service/upload/upload-img.service';
import { BaseTiny } from 'app/core/base/tinymce';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CourseActivityService } from 'app/core/service/subject/course-activity.service';
import { CourseService } from 'app/core/service/subject/course.service';
import { QuestionsService } from 'app/core/service/subject/questions.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pre-post',
  templateUrl: './pre-post.component.html',
  styleUrls: ['./pre-post.component.scss']
})
export class PrePostComponent extends BaseTiny implements OnInit {

  questions = {
    status_id: 1,
    created_by: null,
    created_datetime: null,
    updated_by: null,
    updated_datetime: null,
    search: null,
    owner_agency_uid: null,
    question_uid: null,
    question_name: null,
    correct_answer: null,
    correct_choice_uid: null,
    course_activity_uid: null,
    choices:null
  }
  choices={
    status_id: 1,
    created_by: null,
    created_datetime: null,
    updated_by: null,
    updated_datetime: null,
    search: null,
    owner_agency_uid: null,
    choice_uid: null,
    choice_name: null,
    question_uid: null

  }
  form
  course_activity_uid
  items: any = [{ 1: 1 }]
  displayedColumns = ['1', '2', '3', '4', '5']
  constructor(
    public uploadSV: UploadImgService,
    private coruseSV: CourseService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private courseACSV: CourseActivityService,
    private quSV: QuestionsService
  ) {
    super(uploadSV);
  }

  ngOnInit(): void {
    if (this.quSV.qu_uid != null){
      console.log("x")
      this.quSV.get(this.quSV.qu_uid).subscribe((x: any) => {
        console.log(x)
        this.questions = x
        console.log(this.questions)
      })
    }else{
      console.log(this.coruseSV.quiz)
      this.form = this.coruseSV.quiz
      // this.form.questions = this.questions
      this.coruseSV.lesson.course_activity_uid = this.course_activity_uid
      console.log(this.course_activity_uid)
      // this.quSV.queryString(this.course_activity_uid).subscribe((x: any) => {
      //   console.log(x)
      // })
    }
  }

  savech() {
    if (this.quSV.qu_uid != null) {
      console.log(this.questions)
      // this.quSV.put(this.questions).subscribe((x) => {
      //   Swal.fire({
      //     icon: 'success',
      //     text: 'บันทึกข้อมูลเรียบร้อย',
      //     confirmButtonText: `ตกลง`,
      //   }).then((result) => {
      //     if (result.isConfirmed) {
      //       this.router.navigate(['/app/structure-subject/inform-quiz'])
      //       this.coruseSV.lesson = this.questions
      //       this.cdRef.detectChanges()
      //     }
      //   })
      // })
    }else{
      this.questions.course_activity_uid = this.form
      console.log(this.questions)
      this.quSV.put(this.questions).subscribe((x) => {
        Swal.fire({
          icon: 'success',
          text: 'บันทึกข้อมูลเรียบร้อย',
          confirmButtonText: `ตกลง`,
        }).then((result) => {
          if (result.isConfirmed) {
            this.cdRef.detectChanges()
          }
        })
      })
    }
  }

  edit(id) {

  }

  delete(id) {

  }
}
