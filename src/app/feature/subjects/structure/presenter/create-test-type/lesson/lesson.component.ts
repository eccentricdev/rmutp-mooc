import { courses } from './../../../../../../mock-api/apps/academy/data';
import { BaseTiny } from './../../../../../../core/base/tinymce';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UploadImgService } from 'app/core/service/upload/upload-img.service';
import { CourseService } from 'app/core/service/subject/course.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { CourseActivityService } from 'app/core/service/subject/course-activity.service';

@Component({
  selector: 'app-lesson',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.scss']
})
export class LessonComponent extends BaseTiny implements OnInit {

  form
  datainternal={
    course_activity_name_en:null
  }
  constructor(
    public uploadSV: UploadImgService,
    private coruseSV: CourseService,
    private router:Router,
    private cdRef: ChangeDetectorRef,
    private courseACSV:CourseActivityService
  ) {
    super(uploadSV);
  }

  ngOnInit(): void {
    console.log(this.coruseSV.lesson)
    this.form = this.coruseSV.lesson
  }

  save(){
    this.form.course_activity_name_en = this.datainternal.course_activity_name_en
    console.log(this.form)
      this.courseACSV.put(this.form).subscribe((x)=>{
        console.log(x)
        Swal.fire({
          icon: 'success',
          text: 'บันทึกข้อมูลเรียบร้อย',
          confirmButtonText: `ตกลง`,
        }).then((result) => {
          if (result.isConfirmed) {
            this.router.navigate(['/app/subject'])
          }
        })
      })
  }

  clear(){
    this.router.navigate(['/app/subject'])
  }
}
