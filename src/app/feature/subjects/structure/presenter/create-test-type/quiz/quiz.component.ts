import { BaseForm } from './../../../../../../core/base/base-form';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseTiny } from 'app/core/base/tinymce';
import { CourseActivityService } from 'app/core/service/subject/course-activity.service';
import { CourseService } from 'app/core/service/subject/course.service';
import { QuestionsService } from 'app/core/service/subject/questions.service';
import { UploadImgService } from 'app/core/service/upload/upload-img.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent extends BaseTiny implements OnInit {

  form
  course_activity_uid
  questions = {
    status_id: 1,
    created_by: null,
    created_datetime: null,
    updated_by: null,
    updated_datetime: null,
    search: null,
    owner_agency_uid: null,
    question_uid: null,
    question_name: null,
    correct_answer: null,
    correct_choice_uid: null,
    course_activity_uid: null,
    choices: null
  }
  constructor(
    public uploadSV: UploadImgService,
    private coruseSV: CourseService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private courseACSV: CourseActivityService,
    private quSV: QuestionsService
  ) {
    super(uploadSV);
  }

  ngOnInit(): void {
    if (this.quSV.qu_uid != null) {
      console.log("x")
      this.quSV.get(this.quSV.qu_uid).subscribe((x: any) => {
        console.log(x)
        this.questions = x
        console.log(this.questions)
      })
    } else {
      console.log(this.coruseSV.quiz)
      this.form = this.coruseSV.quiz
      // this.form.questions = this.questions
      this.coruseSV.lesson.course_activity_uid = this.course_activity_uid
      console.log(this.course_activity_uid)
      this.quSV.queryString(this.course_activity_uid).subscribe((x: any) => {
        console.log(x)
      })
    }

  }

  save() {
    // this.form.questions = this.questions
    if (this.quSV.qu_uid != null) {
      console.log(this.questions)
      this.quSV.put(this.questions).subscribe((x) => {
        Swal.fire({
          icon: 'success',
          text: 'บันทึกข้อมูลเรียบร้อย',
          confirmButtonText: `ตกลง`,
        }).then((result) => {
          if (result.isConfirmed) {
            this.router.navigate(['/app/structure-subject/inform-quiz'])
            this.coruseSV.lesson = this.questions
            this.cdRef.detectChanges()
          }
        })
      })
    }else{
      this.questions.course_activity_uid = this.form
      console.log(this.questions)
      this.quSV.put(this.questions).subscribe((x) => {
        Swal.fire({
          icon: 'success',
          text: 'บันทึกข้อมูลเรียบร้อย',
          confirmButtonText: `ตกลง`,
        }).then((result) => {
          if (result.isConfirmed) {
            this.router.navigate(['/app/structure-subject/inform-quiz'])
            this.coruseSV.lesson = this.questions
            this.cdRef.detectChanges()
          }
        })
      })
    }
    
  }

  clear() {
    this.router.navigate(['/app/subject'])
  }


}

