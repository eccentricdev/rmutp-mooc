import { courses } from './../../../../../mock-api/apps/academy/data';
import { activities } from './../../../../../mock-api/pages/activities/data';

import { BaseForm } from './../../../../../core/base/base-form';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { CourseService } from 'app/core/service/subject/course.service';
import { SubjectService } from 'app/core/service/subject/subject.service';
import { ActivityService } from 'app/core/service/subject/activity.service';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { catchError, concatMap, map, tap } from 'rxjs/operators';
import { Location } from '@angular/common';
import { throwError } from 'rxjs';
import { SweetalertService } from 'app/core/service/sweetalert.service';
import { CourseActivityService } from 'app/core/service/subject/course-activity.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-structure',
  templateUrl: './create-structure.component.html',
  styleUrls: ['./create-structure.component.scss']
})
export class CreateStructureComponent extends BaseForm implements OnInit {

  activity
  form: FormGroup;
  constructor(
    private swSV: SweetalertService,
    public location: Location,
    public activeRouter: ActivatedRoute,
    public fb: FormBuilder,
    private router: Router,
    private subjectSV: SubjectService,
    private coruseSV: CourseService,
    private activitySV: ActivityService,
    private coursesAcSV:CourseActivityService,
    private cdRef: ChangeDetectorRef,
  ) {
    super(fb, activeRouter);
    this.form = this.fb.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      search: null,
      owner_agency_uid: null,
      course_uid: null,
      subject_uid: null,
      course_code: null,
      course_short_name_en: null,
      course_short_name_th: null,
      course_name_th: null,
      course_name_en: null,
      course_image: null,
      course_activitys: this.fb.array([

      ]),
    })
  }

  createSubForm(value?) {
    if (value) {
      return this.fb.group({
        status_id: value.status_id,
        created_by: value.created_by,
        created_datetime: value.created_datetime,
        updated_by: value.updated_by,
        updated_datetime: value.updated_datetime,
        search: value.search,
        owner_agency_uid: value.owner_agency_uid,
        course_activity_uid: value.course_activity_uid,
        course_activity_code: value.course_activity_code,
        course_activity_short_name_en: value.course_activity_short_name_en,
        course_activity_short_name_th: value.course_activity_short_name_th,
        course_activity_name_th: value.course_activity_name_th,
        course_activity_name_en: value.course_activity_name_en,
        course_activity_image: value.course_activity_image,
        source_learning_video: value.source_learning_video,
        is_pretest: value.is_pretest,
        is_postest: value.is_postest,
        quiz_test_time: value.quiz_test_time,
        quiz_question_number: value.quiz_question_number,
        pre_test_time: value.pre_test_time,
        pre_test_question_number: value.pre_test_question_number,
        post_test_time: value.post_test_time,
        post_test_question_number: value.post_test_question_number,
        activity_uid: value.activity_uid,
        course_uid: value.course_uid,
        questions: value.questions
      })
    }
    return this.fb.group({
      status_id: null,
      created_by: null,
      created_datetime: null,
      updated_by: null,
      updated_datetime: null,
      search: null,
      owner_agency_uid: null,
      course_activity_uid: null,
      course_activity_code: null,
      course_activity_short_name_en: null,
      course_activity_short_name_th: null,
      course_activity_name_th: null,
      course_activity_name_en: null,
      course_activity_image: null,
      source_learning_video: null,
      is_pretest: null,
      is_postest: null,
      quiz_test_time: null,
      quiz_question_number: null,
      pre_test_time: null,
      pre_test_question_number: null,
      post_test_time: null,
      post_test_question_number: null,
      activity_uid: null,
      course_uid: null,
      questions: this.fb.array([

      ]),
    })
  }

  _createSubForm(value?) {
    if (value) {
      return this.fb.group({
        status_id: null,
        created_by: null,
        created_datetime: null,
        updated_by: null,
        updated_datetime: null,
        search: null,
        owner_agency_uid: null,
        question_uid: null,
        question_name: null,
        correct_answer: null,
        correct_choice_uid: null,
        course_activity_uid: null,
        choices: null
      })
    }
    return this.fb.group({
      status_id: null,
        created_by: null,
        created_datetime: null,
        updated_by: null,
        updated_datetime: null,
        search: null,
        owner_agency_uid: null,
        question_uid: null,
        question_name: null,
        correct_answer: null,
        correct_choice_uid: null,
        course_activity_uid: null,
        choices: null
    })
  }
  addForm() {
    console.log(this.form.value)
    if (this.form.value.course_activitys == null) {
      console.log("course_activitys == null")
    } else {
      console.log("course_activitys == null")
      console.log(this.state)
      switch (this.state) {
        case 'edit':

          this.coruseSV.put(this.form.getRawValue()).pipe(
            tap(res => console.log(res)),
            // tap(res => this.swSV.saveSuccess()),
            // tap(() => this.location.back()),
            catchError(err => {
              this.swSV.error()
              return throwError(err)
            })
          ).subscribe()
          break;

        case 'add':
          console.log(this.form.value)
          if (this.form.value.course_activitys.length == 0) {
            console.log("bbdkhfb")
            this.coruseSV.add(this.form.getRawValue()).pipe(
              tap(res => console.log(res)),
              // tap(res => this.swSV.saveSuccess()),
              
              tap((res:any) => this.router.navigate(['/app/structure-subject/structure/edit',res.course_uid])),
              tap(() => this.cdRef.detectChanges()),
              catchError(err => {
                this.swSV.error()
                return throwError(err)
              })
            ).subscribe()
            this.cdRef.detectChanges()
          }

          break
      }
    }
    let form = this.form.get('course_activitys') as FormArray
    form.push(this.createSubForm())

  }

  removeForm(index: number) {
    let form = this.form.get('course_activitys') as FormArray
    console.log(form.value)
    console.log(this.form.value.course_activitys[index].course_activity_uid)
    let id = this.form.value.course_activitys[index].course_activity_uid
    if(id){
      this.coursesAcSV.deleteDate(id).subscribe(() => {
        // Swal.fire('ลบข้อมูลเรียบร้อยแล้ว', '', 'success');
        // this.items = this.subjectSV.getAll();
        form.removeAt(index)
        // this.cdRef.detectChanges()
      });
    }else{
      form.removeAt(index)
    }
  
  }

  ngOnInit(): void {
    console.log(this.subjectSV.Subjectuid)
    console.log(this.form.value)
    this.activitySV.getAll().subscribe((x:any) => {
      this.activity = x.filter(status => status.status_id == 1)
    })
    switch (this.state) {
      case 'edit':
        console.log(this.id)
        this.callType().pipe(
          concatMap(() => {
            return this.coruseSV.get(this.id).pipe(
              tap(res => console.log(res)),
              tap((res: any) => {
                this.form.patchValue(res)
                // this.form.setControl('course_activitys',this.fb.array([this.createSubForm(res)]))
                let form = this.form.get('course_activitys') as FormArray
                res.course_activitys.forEach(element => {
                  form.push(this.createSubForm(element))
                });
              })
            )
          })
        ).subscribe()
        break;

      case 'add':
        this.addForm()
        break
    }
  }



  add(item) {
    switch (this.state) {
      case 'edit':

        this.coruseSV.put(this.form.getRawValue()).pipe(
          tap(res => console.log(res)),
          // tap(res => this.swSV.saveSuccess()),
          // tap(() => this.location.back()),
          catchError(err => {
            this.swSV.error()
            return throwError(err)
          })
        ).subscribe()
        break;

      case 'add':
        console.log(this.form.value)
        if (this.form.value.course_activitys != null) {
          this.coruseSV.add(this.form.getRawValue()).pipe(
            tap(res => console.log(res)),
            // tap(res => this.swSV.saveSuccess()),
            // tap(() => this.location.back()),
            catchError(err => {
              this.swSV.error()
              return throwError(err)
            })
          ).subscribe()
        }

        break;
      }
    console.log(item.value)
    if (item.value.activity_uid == "f3d247be-e4f2-47ce-b94e-48e2de9f526f") {
      //lesson
      this.coruseSV.lesson = item.value
      this.router.navigate(['/app/structure-subject/lesson'])
    }
    else if (item.value.activity_uid == "3e47355d-c02a-4e30-8a9d-ead359a8cb96") {
      //Quiz
      this.coruseSV.lesson = item.value
      this.router.navigate(['/app/structure-subject/inform-quiz'])
    } else if (item.value.activity_uid == "3e4d00e3-1c9d-4778-94b7-cf6894de9827") {
      //pre-post
      this.coruseSV.lesson = item.value
      this.router.navigate(['/app/structure-subject/inform'])
    }

  }

  search(item) {
    console.log(item.value)
    if (item.value.activity_uid == "f3d247be-e4f2-47ce-b94e-48e2de9f526f") {
      //lesson
      this.coruseSV.lesson = item.value
      this.router.navigate(['/app/structure-subject/lesson'])
    }
    else if (item.value.activity_uid == "3e47355d-c02a-4e30-8a9d-ead359a8cb96") {
      //Quiz
      this.coruseSV.lesson = item.value
      this.router.navigate(['/app/structure-subject/inform-quiz'])
    } else if (item.value.activity_uid == "3e4d00e3-1c9d-4778-94b7-cf6894de9827") {
      //pre-post
      this.coruseSV.lesson = item.value
      this.router.navigate(['/app/structure-subject/inform'])
    }
  }

  callType() {
    return this.activitySV.getAll().pipe(
      map((res: any) => res.filter(r => r.status_id == 1)),
      tap((res: any) => this.activity = [...res])
    )
  }

  back(){
    this.router.navigate(['/app/structure-subject/list-structure'])
  }
}
