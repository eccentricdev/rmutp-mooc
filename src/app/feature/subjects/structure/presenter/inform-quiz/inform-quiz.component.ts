import { CourseService } from 'app/core/service/subject/course.service';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseList } from 'app/core/base/base-list';
import { CourseActivityService } from 'app/core/service/subject/course-activity.service';
import { QuestionsService } from 'app/core/service/subject/questions.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-inform-quiz',
  templateUrl: './inform-quiz.component.html',
  styleUrls: ['./inform-quiz.component.scss']
})
export class InformQuizComponent extends BaseList implements OnInit {

  itemDatas
  form
  // itemDatas:any = [{1:1}]
  displayedColumns = ['1','2','3']
  constructor(
    private coruseSV: CourseService,
    private router:Router,
    private cdRef: ChangeDetectorRef,
    private courseACSV:CourseActivityService,
    private quSV:QuestionsService
  ) {
    super();
  }

  ngOnInit(): void {
    console.log(this.coruseSV.lesson)
    this.form = this.coruseSV.lesson
    this.courseACSV.get(this.form.course_activity_uid).subscribe((x:any)=>{
      console.log(x)
      this.itemDatas = x.questions
    })
  }

  addQu(){
    this.quSV.qu_uid = null
    this.coruseSV.quiz = this.form.course_activity_uid
    this.router.navigate(['/app/structure-subject/quiz'])
  }

  save(){
    console.log(this.form)
    this.courseACSV.put(this.form).subscribe((x)=>{
      console.log(x)
      Swal.fire({
        icon: 'success',
        text: 'บันทึกข้อมูลเรียบร้อย',
        confirmButtonText: `ตกลง`,
      }).then((result) => {
        if (result.isConfirmed) {
          this.router.navigate(['/app/structure-subject'])
        }
      })
    })
    
  }

  clear(){
    this.router.navigate(['/app/structure-subject'])
  }

  edit(id){
    this.quSV.qu_uid = id
    this.router.navigate(['/app/structure-subject/quiz'])

  }

  delete(id){
    Swal.fire({
      icon: 'error',
      title: 'คุณต้องการลบข้อมูลนี้หรือไม่?',
      showCancelButton: true,
      confirmButtonText: `ตกลง`,
      cancelButtonText: `ยกเลิก`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.quSV.deleteDate(id).subscribe(() => {
          Swal.fire('ลบข้อมูลเรียบร้อยแล้ว', '', 'success');
          this.courseACSV.get(this.form.course_activity_uid).subscribe((x:any)=>{
            console.log(x)
            this.itemDatas = x.questions
          }),
          this.cdRef.detectChanges()
        });
      }
    });
  }
}
