import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformQuizComponent } from './inform-quiz.component';

describe('InformQuizComponent', () => {
  let component: InformQuizComponent;
  let fixture: ComponentFixture<InformQuizComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformQuizComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformQuizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
