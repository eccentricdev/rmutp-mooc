import { SubjectService } from './../../../../../core/service/subject/subject.service';
import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { BaseList } from 'app/core/base/base-list';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseList implements OnInit,OnChanges {

  @Input() items: any
  itemDatas:any = [{1:1}]
  displayedColumns = ['1','2','3','4','5','6']
  constructor(
    private router:Router,
    private cdRef: ChangeDetectorRef,
    private subjectSV:SubjectService
  ) { 
    super()
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes?.items?.currentValue) {
      this.items = this.updateMatTable(changes.items.currentValue)
    } 
  }

  edit(id){
    console.log(id)
    this.subjectSV.Subjectuid = id
    this.router.navigate(['/app/structure-subject/list-structure'])
  }

  delete(id){
    Swal.fire({
      icon: 'error',
      title: 'คุณต้องการลบข้อมูลนี้หรือไม่?',
      showCancelButton: true,
      confirmButtonText: `ตกลง`,
      cancelButtonText: `ยกเลิก`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.subjectSV.deleteDate(id).subscribe(() => {
          Swal.fire('ลบข้อมูลเรียบร้อยแล้ว', '', 'success');
          this.items = this.subjectSV.getAll();
          this.cdRef.detectChanges()
        });
      }
    });
  }

}
