import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContainerComponent } from './container/container.component';
import { SearchComponent } from './presenter/search/search.component';
import { ListComponent } from './presenter/list/list.component';
import { ListpersubjcetComponent } from './presenter/listpersubjcet/listpersubjcet.component';
import { CreateStructureComponent } from './presenter/create-structure/create-structure.component';
import { FormInformComponent } from './presenter/form-inform/form-inform.component';
import { PrePostComponent } from './presenter/create-test-type/pre-post/pre-post.component';
import { LessonComponent } from './presenter/create-test-type/lesson/lesson.component';
import { QuizComponent } from './presenter/create-test-type/quiz/quiz.component';
import { SharedModule } from 'app/shared/shared.module';
import { InformQuizComponent } from './presenter/inform-quiz/inform-quiz.component';

const routes:Routes = [
  {
    path:'',
    component:ContainerComponent
  },
  {
    path:'add-structure',
    component:CreateStructureComponent
  },
  {
    // path:'edit/:id',
    path:'list-structure',
    component:ListpersubjcetComponent
  },
  {
    // path:'edit/:id',
    path:'structure/edit/:id',
    component:CreateStructureComponent
  },
  {
    path:'inform',
    component:FormInformComponent
  },
  {
    path:'pre-post',
    component:PrePostComponent
  },
  {
    path:'lesson',
    component:LessonComponent
  },
  {
    path:'inform-quiz',
    component:InformQuizComponent
  },
  {
    path:'quiz',
    component:QuizComponent
  },
  // {
  //   path:'quiz/edit/:id',
  //   component:QuizComponent
  // },
  
]

@NgModule({
  declarations: [
    ContainerComponent,
    SearchComponent,
    ListComponent,
    ListpersubjcetComponent,
    CreateStructureComponent,
    FormInformComponent,
    PrePostComponent,
    LessonComponent,
    QuizComponent,
    InformQuizComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class StructureModule { }
