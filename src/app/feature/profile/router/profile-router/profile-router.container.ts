import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-router',
  template: '<router-outlet></router-outlet>'
})
export class ProfileRouterContainer implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
