import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ProfileContainerComponent } from '../../container/profile-container/profile-container.component';
import { PetitionComponent } from '../components/model/petition/petition.component';

@Component({
  selector: 'app-profile-information',
  templateUrl: './profile-information.component.html',
  styleUrls: ['./profile-information.component.scss']
})
export class ProfileInformationComponent implements OnInit {
  @Input() information
  @Input() subject = []
  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  open(){
    const dialogRef = this.dialog.open(PetitionComponent, {
      width: '550px',
      
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
  }

}
