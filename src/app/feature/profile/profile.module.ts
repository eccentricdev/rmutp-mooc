import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRouterContainer } from './router/profile-router/profile-router.container';
import {  RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ProfileInformationComponent } from './presenter/profile-information/profile-information.component';
import { ProfileContainerComponent } from './container/profile-container/profile-container.component';
import { PetitionComponent } from './presenter/components/model/petition/petition.component';

const routes: Routes = [
  {
    path:'',
    component: ProfileRouterContainer,
    children: [
      {
        path:'',
        component: ProfileContainerComponent
      }
    ]
  }
]


@NgModule({
  declarations: [
    ProfileRouterContainer,
    ProfileInformationComponent,
    ProfileContainerComponent,
    PetitionComponent,    
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class ProfileModule { }
