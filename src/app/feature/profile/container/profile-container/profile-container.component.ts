import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { TestSubjectService } from 'app/core/service/subject/test-subject.service';
import { selectInformationSubject, selectinformationUser } from 'app/core/state/app-state/selector/app-state.selectors';
import { AppState } from 'app/state/reducers';
import { Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-profile-container',
  templateUrl: './profile-container.component.html',
  styleUrls: ['./profile-container.component.scss']
})
export class ProfileContainerComponent implements OnInit {
  information$  = new Observable()
  completeSubject$ = new Observable()
  constructor(
    private store: Store<AppState>,
    private testSubjectSV: TestSubjectService
  ) { 
    this.information$ = this.store.select(selectinformationUser)
    this.completeSubject$ = this.store.select(selectinformationUser).pipe(
      switchMap(res => this.testSubjectSV.queryString(`user_uid=${res.user_uid}&is_complete=true`))
    )
  }

  ngOnInit(): void {
  }

}
