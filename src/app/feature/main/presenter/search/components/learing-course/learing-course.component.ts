import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import { CONTINUES_TEST } from 'app/core/state/app-state/action/app-state.actions';
import { AppState } from 'app/state/reducers';

@Component({
  selector: 'app-learing-course',
  templateUrl: './learing-course.component.html',
  styleUrls: ['./learing-course.component.scss']
})
export class LearingCourseComponent implements OnInit, OnChanges {
  @Input() subject: any[] = []
  displaySubject = []
  startIndex = 0;
  endIndex = 4
  constructor(
      private store:Store<AppState>,
  ) { }


  ngOnChanges(changes: SimpleChanges): void {
    if('subject' in changes && this.subject){
        let display = [...this.subject]
        this.displaySubject = [...display].slice(this.startIndex,this.endIndex)
        
    }
  }

  ngOnInit(): void {
  }

  continuseInformationSubject(subject){
    this.store.dispatch(CONTINUES_TEST({ subjectUid: subject.subject_uid }))
  }

  nextto(){
    let lastIndex = this.displaySubject.length - 1
    let lastElemen = this.displaySubject[lastIndex]
    let lastElementIndex = this.subject.findIndex(d => d.subject_uid == lastElemen.subject_uid)
    let nextElement 
    if(lastElementIndex == (this.subject.length - 1)) {        
        nextElement = this.subject[0]
        this.displaySubject =  [...this.displaySubject ,nextElement]
    } else {
        let nextElementIndex = this.subject.findIndex(s => s.subject_uid == lastElemen.subject_uid)
        nextElementIndex ++
        nextElement = this.subject[nextElementIndex]
        this.displaySubject = [...this.displaySubject , nextElement]
    }
    
    this.displaySubject.shift()
  }

  back(){
    
    let firstElement = this.displaySubject[0]
    let firstElementIndex = this.subject.findIndex(s => s.subject_uid == firstElement.subject_uid)
    console.log(firstElementIndex)
    let nextElement
    if(firstElementIndex == 0){
        let lastElementIndex = this.subject.length - 1
        nextElement = this.subject[lastElementIndex]
        this.displaySubject = [nextElement , ...this.displaySubject] 
    } else {
      firstElementIndex --
      nextElement = this.subject[firstElementIndex]
      this.displaySubject = [nextElement , ...this.displaySubject] 
    }



    this.displaySubject.pop()
  }


}
