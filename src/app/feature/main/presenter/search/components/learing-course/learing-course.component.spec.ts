import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LearingCourseComponent } from './learing-course.component';

describe('LearingCourseComponent', () => {
  let component: LearingCourseComponent;
  let fixture: ComponentFixture<LearingCourseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LearingCourseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LearingCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
