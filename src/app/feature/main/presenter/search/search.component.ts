import { debounceTime, filter, map, switchMap, tap } from 'rxjs/operators';
import { SubjectService } from './../../../../core/service/subject/subject.service';
import { Router } from '@angular/router';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CourseService } from 'app/core/service/subject/course.service';
import { Store } from '@ngrx/store';
import { AppState } from 'app/state/reducers';
import { SELECT_SUBJECT } from 'app/core/state/app-state/action/app-state.actions';
import { fromEvent, Observable } from 'rxjs';
import { selectInformationSubject, selectinformationUser, selectIsLogin } from 'app/core/state/app-state/selector/app-state.selectors';
import { TestSubjectService } from 'app/core/service/subject/test-subject.service';
import { MatDialog } from '@angular/material/dialog';
import { SearchSubjectComponent } from 'app/layout/layouts/vertical/classy/modal/search-subject/search-subject.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  displaySubject = []
  subject = []
  startIndex = 0;
  endIndex = 4
  isAuthen$ = new Observable()
  getSubjectUndone$ = new Observable()
  @ViewChild('saerchSubjectInput',{static: true}) input: ElementRef<HTMLInputElement>

  constructor(
    private router:Router,
    private SUBJECTSV:SubjectService,
    private undoneSubjectSV: TestSubjectService,
    private store: Store<AppState>,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.isAuthen$ = this.store.select(selectIsLogin)
    this.getSubjectUndone$ = this.store.select(selectinformationUser).pipe(
      switchMap(res => this.undoneSubjectSV.getUndoneSubject(res.user_uid))
    )
    
    this.SUBJECTSV.getAll().subscribe((x:any)=>{

      this.subject = x
      this.displaySubject = [...this.subject].slice(this.startIndex,this.endIndex)
    })

    fromEvent(this.input.nativeElement,'input').pipe(
          debounceTime(500),
          map((event: any) => event.target.value),
          filter(input => !!input),
      tap(input => {
            this.dialog.open(SearchSubjectComponent,{
                  width: '800px',
                  height: '800px',
                  data: {
                    textInput: input
                  }
            })
          })
    ).subscribe()
  }

  next(subject){    
    this.SUBJECTSV.Subjectdata = subject
    this.store.dispatch(SELECT_SUBJECT({ subject: subject}))
    this.router.navigate(['/app/main/information',subject.subject_uid])
  }






  nextto(){
    let lastIndex = this.displaySubject.length - 1
    let lastElemen = this.displaySubject[lastIndex]
    let lastElementIndex = this.subject.findIndex(d => d.subject_uid == lastElemen.subject_uid)
    let nextElement 
    if(lastElementIndex == (this.subject.length - 1)) {        
        nextElement = this.subject[0]
        this.displaySubject =  [...this.displaySubject ,nextElement]
    } else {
        let nextElementIndex = this.subject.findIndex(s => s.subject_uid == lastElemen.subject_uid)
        nextElementIndex ++
        nextElement = this.subject[nextElementIndex]
        this.displaySubject = [...this.displaySubject , nextElement]
    }
    
    this.displaySubject.shift()
  }

  back(){
    
    let firstElement = this.displaySubject[0]
    let firstElementIndex = this.subject.findIndex(s => s.subject_uid == firstElement.subject_uid)
    console.log(firstElementIndex)
    let nextElement
    if(firstElementIndex == 0){
        let lastElementIndex = this.subject.length - 1
        nextElement = this.subject[lastElementIndex]
        this.displaySubject = [nextElement , ...this.displaySubject] 
    } else {
      firstElementIndex --
      nextElement = this.subject[firstElementIndex]
      this.displaySubject = [nextElement , ...this.displaySubject] 
    }



    this.displaySubject.pop()
  }

}
