import { LanguageService } from './../../../../core/service/subject/language.service';
import { SkillService } from './../../../../core/service/subject/skill.service';
import { SubjectService } from './../../../../core/service/subject/subject.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { switchMap, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from 'app/state/reducers';
import { selectIsLogin } from 'app/core/state/app-state/selector/app-state.selectors';

import { SweetalertService } from 'app/core/service/sweetalert.service';
import { ENROLL } from 'app/core/state/app-state/action/app-state.actions';
import { AppService } from 'app/core/service/app.service';

@Component({
  selector: 'app-imformation',
  templateUrl: './imformation.component.html',
  styleUrls: ['./imformation.component.scss']
})
export class ImformationComponent implements OnInit {
  // @Input() type;
  subject
  skill
  language
  isLogin: boolean =  false
  constructor(
    private router:Router,
    private subjectSV: SubjectService,
    private skillSV:SkillService,
    private languageSV:LanguageService,
    private activeRoute: ActivatedRoute,
    private store: Store<AppState>,
    private appSV: AppService,
    private swalSV: SweetalertService
    
  ) { }

  ngOnInit(): void {

    this.store.select(selectIsLogin).pipe(
      tap(islogin => this.isLogin = islogin)
    ).subscribe()

    this.activeRoute.params.pipe(
      switchMap(({ subject_uid }) => this.subjectSV.get(subject_uid)),
      tap(subject => this.subject = subject)
    ).subscribe()
    this.skillSV.getAll().subscribe((x)=>{
      this.skill = x
    })
    this.languageSV.getAll().subscribe((x)=>{
      this.language = x
    })
  }

  enroll(){
    if(this.isLogin) {
      this.subjectSV.type = 'preInform'
      this.router.navigate(['/app/main/enroll', this.subject.subject_uid])
      this.store.dispatch(ENROLL({ subjectUid: this.subject.subject_uid }))
      return 

    }

    this.appSV.openDialogLogin()

    // this.swalSV.warning('กรุณาเข้าสู่ระบบก่อนลงทะเบียนเรียน')
  }
}
