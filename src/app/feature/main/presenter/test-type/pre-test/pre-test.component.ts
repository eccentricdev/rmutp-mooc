import { Component, OnInit, OnDestroy, ChangeDetectorRef, Input, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import { CourseActivityService } from 'app/core/service/subject/course-activity.service';
import { SUBMIT_TEST, UPDATE_STATE_RADIO } from 'app/core/state/app-state/action/app-state.actions';

import { ActivityType, StartTest } from 'app/core/type';
import { AppState } from 'app/state/reducers';
import { interval, Observable, Subscription, timer } from 'rxjs';
import { map, take, takeWhile, tap } from 'rxjs/operators';

@Component({
  selector: 'app-pre-test',
  templateUrl: './pre-test.component.html',
  styleUrls: ['./pre-test.component.scss']
})
export class PreTestComponent implements OnInit,OnChanges,OnDestroy {
  @Output() next = new EventEmitter()
  @Input() test = null
  @Input() selectActivity
  @Output() onSubmit = new EventEmitter()
  @Output() onTimeOut = new EventEmitter()
  question = null
  type = 'pre-test'
  check = false
  checktest = false
  countDown$ = new Observable()
  totalSecondTime: number = -1
  SECOND_COUNTDOWN: number = 1000;
  SECOND: number = 1000
  MINUTE: number = 60

  isStartTest: boolean
  constructor(
    private store: Store<AppState>
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if('selectActivity' in changes){
      this.isStartTest = this.selectActivity.is_complete || false
      this.totalSecondTime = (this.selectActivity?.test_time * this.MINUTE)
      
    }
  }

  ngOnInit(): void {    
    this.setCountDown()
  }

  ngOnDestroy() {
    
  }

  setCountDown() {
    this.countDown$ = timer(0, this.SECOND_COUNTDOWN).pipe(
      take(this.totalSecondTime),
      map(() => --this.totalSecondTime),
      takeWhile((time) =>  time != 0,true),
      tap((cnt: number) => {
        if (cnt == 0) {
            // alert('do something')
            this.onTimeOut.emit(true)
        } 
        
      })
    ); 
  }

  

  onNext(){
    this.next.emit(true)  
  }

  selectionChange(event,question){
    this.store.dispatch(UPDATE_STATE_RADIO({ choiceUid:event.value, question}))
  }

  submitTest(){
    this.onSubmit.next(true)
  }
}
