import { BaseTiny } from 'app/core/base/tinymce';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { UploadImgService } from 'app/core/service/upload/upload-img.service';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'app/state/reducers';
import { SUBMIT_TEST, UPDATE_STATE_INPUT } from 'app/core/state/app-state/action/app-state.actions';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent extends BaseTiny implements OnInit,OnChanges {
  @Output() next = new EventEmitter()
  @Input() test = null
  @Input() selectActivity
  @Output() onSubmit = new EventEmitter()
  type  = 'quiz'
  informationQuiz$ = new Observable()
  modTest

  awsnerQuestion = []

  stringTemplate: string = ''
  isStartTest: boolean
  constructor(
    public uploadSV:UploadImgService,
    private store: Store<AppState>
    
  ) {
    super(uploadSV);
  }

  
  ngOnChanges(changes: SimpleChanges): void {    
    if('selectActivity' in changes){
      this.isStartTest = this.selectActivity.is_complete || false
    }

    let data =  JSON.stringify(this.test)
    this.awsnerQuestion = [...JSON.parse(data)]
    
    
  }

  ngOnInit(): void {


  }

  onInput(event,question){
    if(event){
      this.store.dispatch(UPDATE_STATE_INPUT({ answer: event ,question: question  }))    
    }            
  }

  trackById(index, q) {
    return q.question_uid
  }

  onNext(){
    this.next.emit(true)
  }


  submitTest(){
    this.onSubmit.next(true)
  }

}
