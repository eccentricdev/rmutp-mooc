import { Component, OnDestroy, OnInit, ChangeDetectorRef, EventEmitter, Output, Input, SimpleChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import { CourseActivityService } from 'app/core/service/subject/course-activity.service';
import { SUBMIT_TEST, UPDATE_STATE_RADIO } from 'app/core/state/app-state/action/app-state.actions';

import { ActivityType } from 'app/core/type';
import { AppState } from 'app/state/reducers';


import { Observable, timer } from 'rxjs';
import { map, take, takeWhile, tap } from 'rxjs/operators';

@Component({
  selector: 'app-post-test',
  templateUrl: './post-test.component.html',
  styleUrls: ['./post-test.component.scss']
})
export class PostTestComponent implements OnInit, OnDestroy {
  @Output() next = new EventEmitter()
  @Input() test = null
  @Input() selectActivity
  @Output() onSubmit = new EventEmitter()
  @Output() onTimeOut = new EventEmitter()
  question = null
  check = false
  checktest = false
  type = "post-test"
  countDown$ = new Observable()
  totalSecondTime: number = -1
  SECOND_COUNTDOWN: number = 1000;
  SECOND: number = 1000
  MINUTE: number = 60
  isStartTest: boolean
  constructor(
    private cdRef: ChangeDetectorRef,
    private courseActivitySV: CourseActivityService,
    private store: Store<AppState>
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if('selectActivity' in changes){
      this.isStartTest = this.selectActivity.is_complete || false
      this.totalSecondTime = (this.selectActivity?.test_time * this.MINUTE)
      console.log(this.totalSecondTime)
    }
    
  }

  ngOnInit(): void {
    
    this.setCountDown()
  }

  ngOnDestroy() {
    
  }

  selectionChange(event,question){
    this.store.dispatch(UPDATE_STATE_RADIO({ choiceUid:event.value, question}))
  }

  

  setCountDown() {
    this.countDown$ = timer(0, this.SECOND_COUNTDOWN).pipe(
      take(this.totalSecondTime),
      map(() => --this.totalSecondTime),
      takeWhile((time) =>  time != 0,true),
      tap((cnt: number) => {  
          console.log(cnt)
        if (cnt == 0) {
            // alert('do something')
            this.onTimeOut.emit(true)  
        } 
        
      })
    ); 
  
  }


  onNext(){
    this.next.emit(true)  
  }

  submit(){
    this.onSubmit.next(true)    
  }
}
