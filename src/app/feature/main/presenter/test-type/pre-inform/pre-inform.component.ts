import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { SweetalertService } from 'app/core/service/sweetalert.service';
// import { START_TEST } from 'app/core/state/app-state/action/app-state.actions';
import {   selectCurrentTest, selectInformationSubjectTest, selectIsLogin } from 'app/core/state/app-state/selector/app-state.selectors';
import { AppState } from 'app/state/reducers';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-pre-inform',
  templateUrl: './pre-inform.component.html',
  styleUrls: ['./pre-inform.component.scss']
})
export class PreInformComponent implements OnInit {
  type = 'pretest'
  isLogin: boolean = false
  subject$ = new Observable()
  testSubjectInformation$ = new Observable()
  currentTest$ = new Observable()
  
  @Output() onStartTest = new EventEmitter()
  constructor(
    private store: Store<AppState>,
    private swalSV: SweetalertService,
    
  ) { }

  ngOnInit(): void {
    this.currentTest$ = this.store.select(selectCurrentTest)
    this.testSubjectInformation$ = this.store.select(selectInformationSubjectTest)
    
  }
  
  startAssignment(){
    this.onStartTest.emit(true)
    // if(this.isLogin){
    //   this.store.dispatch(START_TEST({ courseActivityUid: ''}))
    //   return 
    // }

    // this.swalSV.warning('กรุณาเข้าสู่ระบบก่อนเริ่มทำแบบทดสอบ')
    
  }

}
