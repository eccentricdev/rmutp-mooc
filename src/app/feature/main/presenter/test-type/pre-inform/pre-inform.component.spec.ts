import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreInformComponent } from './pre-inform.component';

describe('PreInformComponent', () => {
  let component: PreInformComponent;
  let fixture: ComponentFixture<PreInformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreInformComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreInformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
