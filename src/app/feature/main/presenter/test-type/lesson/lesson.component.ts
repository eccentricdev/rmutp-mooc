import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import { CourseActivityService } from 'app/core/service/subject/course-activity.service';
import { SUBMIT_TEST } from 'app/core/state/app-state/action/app-state.actions';

import { AppState } from 'app/state/reducers';




@Component({
  selector: 'app-lesson',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.scss']
})
export class LessonComponent implements OnInit,OnChanges {
  @Output() next = new EventEmitter()
  @Input() test = null
  @Input() selectActivity
  question = null
  type = 'lesson'
  constructor(
    private courseActivitySV: CourseActivityService,
    private store: Store<AppState>
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
  }

  

  ngOnInit(): void {
  }

  onNext(){
    this.next.emit(true)
    this.store.dispatch(SUBMIT_TEST())
    
  }
}
