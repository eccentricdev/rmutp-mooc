import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterSubject'
})
export class FilterSubjectPipe implements PipeTransform {

  transform(source: [], text: string) {
    if(!text) return source    
    return source.filter((x: any) => x.subject_name_th.includes(text))
  }

}
