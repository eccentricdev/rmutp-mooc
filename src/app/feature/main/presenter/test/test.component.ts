import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { InformationSubject } from 'app/core/service/subject/subject.service';
import { SweetalertService } from 'app/core/service/sweetalert.service';
import {  SELECT_TEST_AND_START_TEST, SUBMIT_TEST, SUBMIT_TEST_TIME_OUT } from 'app/core/state/app-state/action/app-state.actions';
import { selectCurrentTest, selectCurrentTestQuestion, selectInformationSubject, selectIsStartTest, selectTest, selectTypeCurrentActivityCode } from 'app/core/state/app-state/selector/app-state.selectors';
import {  TestDetailInformation } from 'app/core/type';
import { AppState } from 'app/state/reducers';
import {  Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/internal/operators/tap';
import {  filter, takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit, OnDestroy {
  panelOpenState = false;
  type = 'preInform'
  testdata
  // @Input() type;

  // subject: MoocSubject = null
  activities = []
  _selectActivity = null
  
  informationSubject$ = new Observable<InformationSubject>()
  currentTest$ = new Observable<any>()
  selectCurrentTest$ = new Observable<any>()
  informationSubject: any = null
  isStartTest: boolean = false
  destroy$ = new Subject()

  constructor(    
    private cdRef: ChangeDetectorRef,
    private store: Store<AppState>,
    private appSV: SweetalertService
  ) { 

    this.currentTest$ = this.store.select(selectCurrentTestQuestion)
    this.selectCurrentTest$ = this.store.select(selectTest)

    this.informationSubject$ = this.store.select(selectInformationSubject).pipe(
      tap(res => this.informationSubject = [...res?.test_subject_information?.test_informations || []])
    )
    this.store.select(selectTypeCurrentActivityCode).pipe(
      tap(acitivityType => this.type = acitivityType?.toLowerCase()),
      takeUntil(this.destroy$)
    ).subscribe()

    this.store.select(selectIsStartTest).pipe(
      tap(isStartTest => this.isStartTest = isStartTest),
      takeUntil(this.destroy$)
    ).subscribe()

    this.store.select(selectCurrentTest).pipe(
      tap(currentTest => this._selectActivity = {...currentTest}),
      // tap(res => console.log(this._selectActivity)),
      takeUntil(this.destroy$)
    ).subscribe()
  }


  ngOnDestroy(): void {
    this.destroy$.next(true)
    this.destroy$.complete()
  }

  ngOnInit(): void {
   
  }



  selectActivity(activity,test){
    this.type = (activity.activity_code as string)?.toLowerCase()
    let req = {...activity}
    this._selectActivity = {
      ...req,
      course_uid: test.course_uid
    }
    
    this.store.dispatch(SELECT_TEST_AND_START_TEST({
      selectTest: this._selectActivity,
  }))
    this.cdRef.detectChanges()
  }

  nextActivity(acivity: TestDetailInformation){
        let testInformationIndex: number = this.informationSubject.findIndex(c => c.course_uid == this._selectActivity.course_uid)        
        let testInformation  = this.informationSubject[testInformationIndex]
        let limitLength = testInformation?.test_detail_informations.length
        let testDetailInformationIndex = testInformation.test_detail_informations.findIndex(x => x.course_activity_uid == this._selectActivity.course_activity_uid)
        if(( testDetailInformationIndex + 1 ) == limitLength){
          testInformationIndex = testInformationIndex + 1
          let nextCourse = this.informationSubject[testInformationIndex]
          console.log(this.informationSubject)

          if(!nextCourse){
            this._selectActivity = {
              ...this._selectActivity,
              ...this.informationSubject[0].test_detail_informations[0],
              course_uid: this.informationSubject[0].course_uid
            }            
          } 

          if(nextCourse) {
             this._selectActivity = {
              ...this._selectActivity,
              ...nextCourse.test_detail_informations[0],
              course_uid: testInformation.course_uid
            }
          }

         
        } 

        if(( testDetailInformationIndex + 1 ) != limitLength) {
          testDetailInformationIndex = testDetailInformationIndex + 1

          this._selectActivity = {
            ...testInformation.test_detail_informations[testDetailInformationIndex],
            course_uid: testInformation.course_uid
          }
        }


        

        this.type = this._selectActivity.activity_code
        this.cdRef.detectChanges()

        this.store.dispatch(SELECT_TEST_AND_START_TEST({
          selectTest: this._selectActivity,
      }))

  }

  submit(){
    this.appSV.confirm('ยืนยันการส่งคำตอบ').pipe(
        filter(event => event.isConfirmed),    
        tap(() => this.store.dispatch(SUBMIT_TEST()))
    ).subscribe()    
  }

  sumbitTimeOut(){
    this.store.dispatch(SUBMIT_TEST_TIME_OUT())
  }

  trackBycourseuId(index, test){
    return test.course_uid
  }

  trackByCourseActivityUid(index, information) {
    return information.course_activity_uid
  }
}
