import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContainerComponent } from './container/container.component';
import { SearchComponent } from './presenter/search/search.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ImformationComponent } from './presenter/imformation/imformation.component';
import { TestComponent } from './presenter/test/test.component';
import { PreTestComponent } from './presenter/test-type/pre-test/pre-test.component';
import { PostTestComponent } from './presenter/test-type/post-test/post-test.component';
import { LessonComponent } from './presenter/test-type/lesson/lesson.component';
import { QuizComponent } from './presenter/test-type/quiz/quiz.component';
import { PreInformComponent } from './presenter/test-type/pre-inform/pre-inform.component';
import { ResultComponent } from './presenter/test-type/result/result.component';
import { LearingCourseComponent } from './presenter/search/components/learing-course/learing-course.component';
import { FilterSubjectPipe } from './presenter/pipe/filter-subject.pipe';

const routes:Routes = [
  {
    path:'',
    component:ContainerComponent
  },
  {
    path:'enroll',
    component:TestComponent
  },
  {
    path:'enroll/:subject_uid',
    component:TestComponent
  },
  {
    path:'information',
    component:ImformationComponent
  },
  {
    path:'information/:subject_uid',
    component:ImformationComponent
  },
]

@NgModule({
  declarations: [
    ContainerComponent,
    SearchComponent,
    ImformationComponent,
    TestComponent,
    PreTestComponent,
    PostTestComponent,
    LessonComponent,
    QuizComponent,
    PreInformComponent,
    ResultComponent,
    LearingCourseComponent,
    FilterSubjectPipe
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class MainModule { }
