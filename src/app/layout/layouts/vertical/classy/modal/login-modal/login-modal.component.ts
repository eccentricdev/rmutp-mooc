import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AUTHEN } from 'app/core/state/app-state/action/app-state.actions';
import { AppState } from 'app/state/reducers';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent implements OnInit {
  request = {
    email: "",
    full_name: "",
    password: ""
  }
  constructor(
    private store: Store<AppState>
  ) { }

  ngOnInit(): void {
  }



  login(){

    let request  = {...this.request}
    this.store.dispatch(AUTHEN({
        payload: request
    }))
  }

}
