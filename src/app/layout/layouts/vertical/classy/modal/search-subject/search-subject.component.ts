import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { SubjectService } from 'app/core/service/subject/subject.service';
import { SELECT_SUBJECT } from 'app/core/state/app-state/action/app-state.actions';
import { AppState } from 'app/state/reducers';
import { startsWith } from 'lodash';
import { fromEvent, Observable, of } from 'rxjs';
import { catchError, debounceTime, delay, map, startWith, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-search-subject',
  templateUrl: './search-subject.component.html',
  styleUrls: ['./search-subject.component.scss']
})
export class SearchSubjectComponent implements OnInit {
  @ViewChild('saerchSubjectInput',{static: true}) input: ElementRef<HTMLInputElement>;
  displaySubject$ = new Observable()
  constructor(
    public dialogRef: MatDialogRef<SearchSubjectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private subjectSV:SubjectService,
    private store: Store<AppState>,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.displaySubject$ = fromEvent(this.input.nativeElement,'input').pipe(
      debounceTime(500),
      startWith(this.data.textInput),
      map(input => input?.target?.value ? input?.target?.value :input),
      switchMap(textInput => this.subjectSV.queryString(`subject_name_th=${textInput}`).pipe(
            catchError(err => {
                      return of([])
                })
        ))
    )    
  }



  selectSubject(subject){
    this.store.dispatch(SELECT_SUBJECT({ subject: subject}))
    this.router.navigate(['/app/main/information',subject.subject_uid])
    this.dialogRef.close()
  }

}
