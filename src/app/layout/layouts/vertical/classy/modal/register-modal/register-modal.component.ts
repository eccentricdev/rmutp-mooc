import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AppService } from 'app/core/service/app.service';
import { RegisterService } from 'app/core/service/auth/register.service';
import { SweetalertService } from 'app/core/service/sweetalert.service';
import { throwError } from 'rxjs';
import { catchError, filter, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.scss']
})
export class RegisterModalComponent implements OnInit {
  @ViewChild('passwordInput', {static: false}) passwordInput: ElementRef<HTMLInputElement>;
  isVisibility: boolean = false
  request = {
    email: null,
    full_name: null,
    password: null,
    faculty_name: null,
    major_name: null
  }
  constructor(
    private registerSV: RegisterService,
    private appSV: SweetalertService,
    public dialogRef: MatDialogRef<RegisterModalComponent>,
  ) { }

  ngOnInit(): void {
  }

  toggleType(){
    this.isVisibility = !this.isVisibility
    if(this.isVisibility){
      this.passwordInput.nativeElement.type = 'text'
    } else {
      this.passwordInput.nativeElement.type = 'password'
    }
  }

  save(){

    this.appSV.confirm('ยืนยันการสมัครสมาชิก').pipe(
      filter(e => e.isConfirmed),
      switchMap(e => this.registerSV.add(this.request).pipe(
            tap(() => this.appSV.saveSuccess('สมัครสมาชิกเรียบร้อยแล้ว')),
            tap(() => this.dialogRef.close()),
            catchError(err => {
                    this.appSV.error('ไม่สามารถสมัครได้')
                    return throwError(err)
                })
      ))
    ).subscribe()
    
  }

}


