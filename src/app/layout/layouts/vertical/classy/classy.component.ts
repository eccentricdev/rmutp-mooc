import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { fromEvent, Observable, Subject } from 'rxjs';
import { debounceTime, takeUntil, tap } from 'rxjs/operators';
import { FuseMediaWatcherService } from '@fuse/services/media-watcher';
import { FuseNavigationService, FuseVerticalNavigationComponent } from '@fuse/components/navigation';
import { InitialData } from 'app/app.types';
import { OdicOpenService } from 'app/core/service/odic/odic-open.service';
import { AppTokenService } from 'app/core/service/token/app-token.service';
import { MatDialog } from '@angular/material/dialog';
import { LoginModalComponent } from './modal/login-modal/login-modal.component';
import { RegisterModalComponent } from './modal/register-modal/register-modal.component';
import { AppState } from 'app/state/reducers';
import { Store } from '@ngrx/store';
import { selectinformationUser, selectIsLogin } from 'app/core/state/app-state/selector/app-state.selectors';
import { LOG_OUT } from 'app/core/state/app-state/action/app-state.actions';
import { AppService } from 'app/core/service/app.service';

@Component({
    selector     : 'classy-layout',
    templateUrl  : './classy.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ClassyLayoutComponent implements OnInit, OnDestroy
{
    data: InitialData;
    isScreenSmall: boolean;
    isLoggedIn: boolean = false
    user$ = new Observable()
    @ViewChild('toolbar',{static: true}) toolbar: ElementRef<HTMLElement>
    
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        
        private _fuseMediaWatcherService: FuseMediaWatcherService,
        private _fuseNavigationService: FuseNavigationService,
        private odicSV: OdicOpenService,
        public _appTokenSV: AppTokenService,
        public dialog: MatDialog,
        private store:Store<AppState>,
        private appService: AppService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for current year
     */
    get currentYear(): number
    {
        return new Date().getFullYear();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {

        this.appService.loginEvent().pipe(
            tap(event => {
                if(event){
                    this.openLoginDialog()
                }
            })
        ).subscribe()

        this.store.select(selectIsLogin).pipe(
            tap(isLogin => this.isLoggedIn = isLogin),
            tap(() => this.dialog.closeAll()),
            takeUntil(this._unsubscribeAll)
        ).subscribe()

        // fromEvent(window,'scroll').pipe(
        //     debounceTime(100),
        //     tap(res => {
        //         // console.log(res)
        //         if(window.pageYOffset == 0){
        //             this.toolbar.nativeElement.classList.add('bg-transparent')
        //             this.toolbar.nativeElement.classList.remove('bg-black')
        //         } else {
        //             this.toolbar.nativeElement.classList.add('bg-black')
        //             this.toolbar.nativeElement.classList.remove('bg-transparent')
        //         }
        //     })
        // ).subscribe()

        this.user$ = this.store.select(selectinformationUser)
        
        this._activatedRoute.data.subscribe((data: Data) => {
            console.log(data)
            this.data = data.initialData;
        });

        // Subscribe to media changes
        this._fuseMediaWatcherService.onMediaChange$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(({matchingAliases}) => {

                // Check if the screen is small
                this.isScreenSmall = !matchingAliases.includes('md');
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle navigation
     *
     * @param name
     */
    toggleNavigation(name: string): void
    {
        // Get the navigation
        const navigation = this._fuseNavigationService.getComponent<FuseVerticalNavigationComponent>(name);

        if ( navigation )
        {
            // Toggle the opened status
            navigation.toggle();
        }
    }

    openLoginDialog() {
        const dialogRef = this.dialog.open(
            LoginModalComponent, {
                width: '450px',
                height: '400px'
            }
        );
    
        // dialogRef.afterClosed().subscribe(result => {
        //   console.log(`Dialog result: ${result}`);
        // });
      }


      openRegisterDialog() {
        const dialogRef = this.dialog.open(RegisterModalComponent,{
            width: '450px',
            height: 'auto'
        });
    
        dialogRef.afterClosed().subscribe(result => {
          console.log(`Dialog result: ${result}`);
        });
      }

    logout(){
        this.store.dispatch(LOG_OUT())
    }
}
