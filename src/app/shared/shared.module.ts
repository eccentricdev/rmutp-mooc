import { SearchAutoPipe } from './pipes/search-auto.pipe';
import { EditorModule } from '@tinymce/tinymce-angular';
import { Injectable, LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DateAdapter, MatNativeDateModule, MAT_DATE_FORMATS, NativeDateAdapter } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import {MatRadioModule} from '@angular/material/radio';
import { PreviewDocumentComponent } from './components/preview-document/preview-document.component';
import { LimitLengthDirective } from './directives/limit-length.directive';
import { TypeOnlyEngDirective } from './directives/type-only-eng.directive';
import { TypeOnlyNumberDirective } from './directives/type-only-number.directive';
import { SafeUrlPipe } from './pipes/safe-url.pipe';
import { ThaidatePipe } from './pipes/thaidate.pipe';
import { NgProgressModule } from 'ngx-progressbar';
import localeTh from '@angular/common/locales/th';
import { GetDataIdPipe } from './pipes/get-data-id.pipe';
import { MatPaginatorModule } from '@angular/material/paginator';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkTreeModule} from '@angular/cdk/tree';
import { FormatTimePipe } from './pipes/timer.pipe';
import {MatExpansionModule} from '@angular/material/expansion';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import {MatRippleModule} from '@angular/material/core';
registerLocaleData(localeTh)
@Injectable()
export class AppDateAdapter extends NativeDateAdapter {
  format(date: Date, displayFormat: Object): string {
    let monthNamesThai = ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.",
        "ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค"];    
    // if (displayFormat === 'input') {
      let day: string = date.getDate().toLocaleString()
      day = +day < 10 ? '0' + day : day;
      // let month: string = (date.getMonth() + 1).toLocaleString()
      // month = +month < 10 ? '0' + month : month;
      let year = date.getFullYear();
      return `${day}/${monthNamesThai[date.getMonth()]}/${year + 543}`;
    // }
    // return date.toDateString();
  }
}

export const PICK_FORMATS = {
  parse: {
    dateInput: {
      month: 'short',
      year: 'numeric',
      day: 'numeric'
    }
  },
  display: {
      dateInput: 'input',
      monthYearLabel: {day: 'numeric', year: 'numeric', month: 'long'},
      dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
      monthYearA11yLabel: {year: 'numeric', month: 'long'}
  }
};
//  format for Moment
export const MY_FORMATS = {
  parse: {
    dateInput: 'DD-MMM-YYYY',
  },
  display: {  
    dateInput: 'DD-MMM-YYYY',
    monthYearLabel: 'DD-MMM-YYYY',
    dateA11yLabel: 'DD-MMM-YYYY',
    monthYearA11yLabel: 'DD-MMM-YYYY',
  },
};


const mat = [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    // MatGridListModule,
    MatTableModule,
    MatIconModule,
    // MatChipsModule,
    MatCheckboxModule,
    MatStepperModule,
    MatRadioModule,
    MatSelectModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    MatMenuModule,
    MatNativeDateModule,
    // MatMomentDateModule,
    NgProgressModule,
    // MatProgressBarModule,
    MatTooltipModule,
    // QRCodeModule,
    MatProgressSpinnerModule,
    MatDialogModule ,
    MatPaginatorModule ,
    EditorModule,
    ScrollingModule,
    CdkTreeModule,
    MatExpansionModule,
    MatRippleModule
  ]

  const pipe = [
    GetDataIdPipe,
    SearchAutoPipe,
    FormatTimePipe,
    SafeHtmlPipe
  ]
  


@NgModule({
    declarations: [
        PreviewDocumentComponent,
        SafeUrlPipe,
        TypeOnlyNumberDirective,
        LimitLengthDirective,
        TypeOnlyEngDirective,
        ThaidatePipe,
        SearchAutoPipe,
        GetDataIdPipe,
        SafeUrlPipe,
        FormatTimePipe,
        SafeHtmlPipe
        // ...pipe
    ],
    imports: [
        ...mat,
        // ...pipe
    ],
    exports: [
        TypeOnlyNumberDirective,
        LimitLengthDirective,
                TypeOnlyEngDirective,
            ThaidatePipe,
            SearchAutoPipe,
            GetDataIdPipe,
            SafeUrlPipe,
            FormatTimePipe,
            SafeHtmlPipe,
        ...mat,
        // ...pipe
    ],
    providers:[
        { provide: LOCALE_ID, useValue: 'th-TH'},
        { provide: MAT_DATE_FORMATS, useValue: PICK_FORMATS},
    { provide: DateAdapter, useClass: AppDateAdapter},
    ]
})
export class SharedModule
{
}
