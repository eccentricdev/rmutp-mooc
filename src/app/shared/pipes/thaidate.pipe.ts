import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'thaidate',
  pure :true
})
export class ThaidatePipe implements PipeTransform {

  transform(value: string): string {
    if(!value) return '-'
    let date = new Date(value).toLocaleDateString('th-TH', {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
    })
    return date
  }

}
