import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PreviewDocumentComponent } from './preview-document.component';

describe('PreviewDocumentComponent', () => {
  let component: PreviewDocumentComponent;
  let fixture: ComponentFixture<PreviewDocumentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
