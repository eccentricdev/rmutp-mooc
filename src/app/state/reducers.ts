

import { ActionReducerMap } from '@ngrx/store';
import { appStateReducer, MoocState } from 'app/core/state/app-state/reducer/app-state.reducer';


export interface AppState {
    appState: MoocState
    
}

export const appReducers: ActionReducerMap<AppState> = {
    appState: appStateReducer
}