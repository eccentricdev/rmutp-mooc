import { UploadImgService } from './../service/upload/upload-img.service';


export class BaseTiny {
  tinyMCE
  protected id: number
  protected state: string
  constructor(
    public uploadSV: UploadImgService,
  ) {
    this.tinyMCE = {
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern autoresize',
      ],
      contextmenu: false,
      document_base_url: 'test',
      video_template_callback: function (data) {
        return '<video width="' + 450 + '" height="' + 225 + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' + '<source src="' + data.source + '"' + (data.sourcemime ? ' type="' + data.sourcemime + '"' : '') + ' />\n' + (data.altsource ? '<source src="' + data.altsource + '"' + (data.altsourcemime ? ' type="' + data.altsourcemime + '"' : '') + ' />\n' : '') + '</video>';
      },
      relative_urls: false,
      remove_script_host: true,
      forced_root_block: false,
      force_br_newlines: true,
      force_p_newlines: false,
      branding: false,
      toolbar:
        'media image code link',
      image_advtab: true,
      file_picker_callback: function (cb, value, meta) {
        console.log(cb);
        console.log(value);
        console.log(meta);
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/* audio/* video/*');
        console.log(input)
        // input.click();
        console.log(111)
        input.onchange = function() {
          let file = input.files[0];
          let reader = new FileReader();
            const formData: FormData = new FormData();
            formData.append('file', file, file.name);
            // let url =   'https://ru.71dev.com/etest-question/documents/46e70137-d577-4013-8c4e-28137a8eb3f3.png'
            // cb(url)
            uploadSV.uploadDocument(formData).subscribe((x: any) => {
              if (x.status === 'success'){
                cb(x.message[0].file_name)
              }
            });
          reader.readAsDataURL(file);
        }
        input.click();
      },
    }
  }
}
