export const enum ActivityType  {
        QUIZ = 'Quiz',
        PRE_TEST = 'pre-test',
        POST_TEST = 'post-test',
        LESSON = 'lesson',
        TYPEINFORM = 'preInform'
}

export interface TestDetailInformation {
    course_activity_uid: string;
    test_question_number: number;
    test_time: number;
    course_activity_name: string;
    activity_code: string;
    is_complete: boolean;
}

export interface TestInformation {
    test_uid: string;
    course_uid: string;
    course_name: string;
    is_complete: boolean;
    test_detail_informations: TestDetailInformation[];
}

export interface TestSubjectInformation {
    subject_name: string;
    is_complete: boolean;
    test_informations: TestInformation[];
}

export interface TestQuestion {
    status_id: number;
    created_by: string;
    created_datetime: string;
    updated_by: string;
    updated_datetime: string;
    search: string;
    owner_agency_uid: string;
    test_question_uid: string;
    test_detail_uid: string;
    question_uid: string;
    selected_choice_uid: string;
    test_question_answer: string;
}

export interface TestDetail {
    status_id: number;
    created_by: string;
    created_datetime: string;
    updated_by: string;
    updated_datetime: string;
    search: string;
    owner_agency_uid: string;
    test_detail_uid: string;
    test_uid: string;
    course_activity_uid: string;
    is_complete: boolean;
    test_questions: TestQuestion[];
}

export interface Test {
    status_id: number;
    created_by: string;
    created_datetime: string;
    updated_by: string;
    updated_datetime: string;
    search: string;
    owner_agency_uid: string;
    test_uid: string;
    test_subject_uid: string;
    course_uid: string;
    remark: string;
    is_complete: boolean;
    test_details: TestDetail[];
}

export interface TestSubject {
    status_id: number;
    created_by: string;
    created_datetime: string;
    updated_by: string;
    updated_datetime: string;
    search: string;
    owner_agency_uid: string;
    test_subject_uid: string;
    user_uid: string;
    subject_uid: string;
    test_subject_no: number;
    test_subject_code: string;
    remark: string;
    is_complete: boolean;
    tests: Test[];
}

export interface Choice {
    choice_uid: string;
    choice_name: string;
}

export interface Question {
    question_uid: string;
    question_name: string;
    selected_choice_uid: string;
    test_question_answer: string;
    choices: Choice[];
}

export interface StartTest {
    test_subject_information: TestSubjectInformation;
    test_subject: TestSubject;
    questions: Question[];
}


// information subject type
export interface TestDetailInformation {
    course_uid: string
    course_activity_uid: string;
    test_question_number: number;
    test_time: number;
    course_activity_name: string;
    activity_code: string;
    is_complete: boolean;
}

export interface TestInformation {
    test_uid: string;
    course_uid: string;
    course_name: string;
    is_complete: boolean;
    test_detail_informations: TestDetailInformation[];
}

export interface TestSubjectInformation {
    subject_name: string;
    is_complete: boolean;
    test_informations: TestInformation[];
}

export interface TestQuestion {
    status_id: number;
    created_by: string;
    created_datetime: string;
    updated_by: string;
    updated_datetime: string;
    search: string;
    owner_agency_uid: string;
    test_question_uid: string;
    test_detail_uid: string;
    question_uid: string;
    selected_choice_uid: string;
    test_question_answer: string;
}

export interface TestDetail {
    status_id: number;
    created_by: string;
    created_datetime: string;
    updated_by: string;
    updated_datetime: string;
    search: string;
    owner_agency_uid: string;
    test_detail_uid: string;
    test_uid: string;
    course_activity_uid: string;
    is_complete: boolean;
    test_questions: TestQuestion[];
}

export interface Test {
    status_id: number;
    created_by: string;
    created_datetime: string;
    updated_by: string;
    updated_datetime: string;
    search: string;
    owner_agency_uid: string;
    test_uid: string;
    test_subject_uid: string;
    course_uid: string;
    remark: string;
    is_complete: boolean;
    test_details: TestDetail[];
}

export interface TestSubject {
    status_id: number;
    created_by: string;
    created_datetime: string;
    updated_by: string;
    updated_datetime: string;
    search: string;
    owner_agency_uid: string;
    test_subject_uid: string;
    user_uid: string;
    subject_uid: string;
    test_subject_no: number;
    test_subject_code: string;
    remark: string;
    is_complete: boolean;
    tests: Test[];
}

export interface Choice {
    choice_uid: string;
    choice_name: string;
}

export interface Question {
    question_uid: string;
    question_name: string;
    selected_choice_uid: string;
    test_question_answer: string;
    choices: Choice[];
}

export interface InformationSubject {
    test_subject_information: TestSubjectInformation;
    test_subject: TestSubject;
    questions: Question[];
}

// information subject type