import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'app/state/reducers';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SelectProgramGuard implements CanActivate {
  private HaveProgram: boolean = false

  constructor(
    private store:Store<AppState>,
    private router:Router
  ){
    this.store.select(selectSelectedProgram).pipe(
      tap(state => this.HaveProgram = !!state),
    ).subscribe()
  }


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (!this.HaveProgram) {
          console.log('redirect from guard')
          this.router.navigate(['app/home'])
          return false
        }

      return true;
  }
  
}
