import { TestBed } from '@angular/core/testing';

import { SelectProgramGuard } from './select-program.guard';

describe('SelectProgramGuard', () => {
  let guard: SelectProgramGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(SelectProgramGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
