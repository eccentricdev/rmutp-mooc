import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'app/core/base/base-service';

@Injectable({
  providedIn: 'root'
})
export class LoginService extends BaseService {

  constructor(
    public http: HttpClient
  ) {
    super('/user/user/login',http);
  }
}
