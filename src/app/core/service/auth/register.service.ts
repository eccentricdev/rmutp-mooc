import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'app/core/base/base-service';
export interface UserDetails {
  
  user_uid: string;
  faculty_name: string;
  major_name: string;
  email: string;
  full_name: string;
  
}

export interface User {
  token: string;
  userDetails: UserDetails;
}

@Injectable({
  providedIn: 'root'
})
export class RegisterService extends BaseService {

  constructor(
    public http: HttpClient
  ) {
    super('/user/user/register',http);
  }
}
