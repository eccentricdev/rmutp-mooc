import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import Swal, { SweetAlertResult } from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SweetalertService {

  constructor() { }

  saveSuccess(msg: string = 'บันทึกสำเร็จแล้ว'){
    Swal.fire({
      icon: 'success',
      text: msg,
      
    })
  }

  updateSuccess(msg: string = 'อัพเดรตสำเร็จแล้ว'){
    Swal.fire({
      icon: 'success',
      text: msg,
    })
  }

  removeSuccess(msg: string = 'ลบรายการสำเร็จแล้ว'){
    Swal.fire({
      icon: 'success',
      text: msg,
    })
  }

  error(msg: string = 'ผิดพลาด'){
    Swal.fire({
      icon: 'error',
      text: msg,
    })
  }
  

  confirm(msg: string = 'ต้องการจะลบข้อมูลนี้ไหม?',confirmButtonText: string = 'ตกลง',cancelButtonText: string = 'ยกเลิก'): Observable<SweetAlertResult<boolean>> {
    let dialog = Swal.fire({
      icon: 'warning',
      title: `${msg}`,
      showCancelButton: true,
      confirmButtonText: `${confirmButtonText}`,
      cancelButtonText: `${cancelButtonText}`,
    })
    return from(dialog)
  }


  warning(msg: string){
    Swal.fire({
      icon: 'warning',
      text: msg,
    })
    
  }
    


  
}
