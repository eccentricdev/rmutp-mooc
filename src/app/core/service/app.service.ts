import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private loginEevn$ = new BehaviorSubject<boolean>(false)
  public searchReport$  = new BehaviorSubject(null)
  constructor() { }


  openDialogLogin(){
    this.loginEevn$.next(true)
  }

  loginEvent(){
    return this.loginEevn$.asObservable()
  }
  
  swaltAlert(title:string = 'บันทึก',text:string = 'บันทึกสำเร็จ') {
    swal.fire({
      text: text,
      icon: 'success',
      confirmButtonText: `ตกลง`,
    })
  }
  swaltAlertError(title:string = 'Error',text:string) {
    swal.fire({
      text: text,
      icon: 'error',
      // confirmButtonText: 'Cool'
    })
  }
}
