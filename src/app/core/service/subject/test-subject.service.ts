import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'app/core/base/base-service';
import { Observable } from 'rxjs';
export interface TestQuestion {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  test_question_uid: string;
  test_detail_uid: string;
  question_uid: string;
  selected_choice_uid: string;
  test_question_answer: string;
}

export interface TestDetail {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  test_detail_uid: string;
  test_uid: string;
  course_activity_uid: string;
  is_complete: boolean;
  test_questions: TestQuestion[];
}

export interface Test {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  test_uid: string;
  test_subject_uid: string;
  course_uid: string;
  remark: string;
  is_complete: boolean;
  test_details: TestDetail[];
}

export interface UndoneTestSubject {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  test_subject_uid: string;
  user_uid: string;
  subject_uid: string;
  test_subject_no: number;
  test_subject_code: string;
  remark: string;
  is_complete: boolean;
  start_test_datetime: string;
  end_test_datetime: string;
  full_name: string;
  phone_number: string;
  faculty_name: string;
  subject_image: string;
  subject_name_th: string;
  tests: Test[];
}
@Injectable({
  providedIn: 'root'
})
export class TestSubjectService extends BaseService {

  constructor(
    public http: HttpClient
  ) { 
    super('/test/test_subject',http)
  }

  getUndoneSubject(userId): Observable<UndoneTestSubject[]>{
    return this.http.get<UndoneTestSubject[]>(`${this.fullUrl}/undone_test_subject/${userId}`)
  }
}
