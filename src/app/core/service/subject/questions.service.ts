import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'app/core/base/base-service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
export interface Choice {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  choice_uid: string;
  choice_name: string;
  question_uid: string;
}

export interface MoocQuestion {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  question_uid: string;
  question_name: string;
  correct_answer: string;
  correct_choice_uid: string;
  course_activity_uid: string;
  choices: Choice[];
}

@Injectable({
  providedIn: 'root'
})
export class QuestionsService extends BaseService {

  qu_uid = null
  constructor(public http : HttpClient) {
    super('/subject/question',http);
  }

  put(data){
    return this.http.put(`https://rmutp.71dev.com/rmutp-mooc-api/api/subject/question`,data)
  }

  getQuestion(questionUid): Observable<MoocQuestion>{
    return this.http.get<MoocQuestion>(`${environment.baseUrl}/api/subject/question/${questionUid}`)

  }
}
