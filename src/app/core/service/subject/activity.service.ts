import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'app/core/base/base-service';

@Injectable({
  providedIn: 'root'
})
export class ActivityService extends BaseService {


  constructor(public http : HttpClient) {
    super('/subject/activity',http);
  }
}
