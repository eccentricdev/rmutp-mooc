import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'app/core/base/base-service';
import { environment } from 'environments/environment';
import { forkJoin, Observable } from 'rxjs';
import { concatMap, map } from 'rxjs/operators';
import { QuestionsService } from './questions.service';
export interface Choice {
        status_id: number;
        created_by: string;
        created_datetime: string;
        updated_by: string;
        updated_datetime: string;
        search: string;
        owner_agency_uid: string;
        choice_uid: string;
        choice_name: string;
        question_uid: string;
    }

    export interface Question {
        status_id: number;
        created_by: string;
        created_datetime: string;
        updated_by: string;
        updated_datetime: string;
        search: string;
        owner_agency_uid: string;
        question_uid: string;
        question_name: string;
        correct_answer: string;
        correct_choice_uid: string;
        course_activity_uid: string;
        choices: Choice[];
    }

    export interface MoocCourseActivity {
        status_id: number;
        created_by: string;
        created_datetime: string;
        updated_by: string;
        updated_datetime: string;
        search: string;
        owner_agency_uid: string;
        course_activity_uid: string;
        course_activity_code: string;
        course_activity_short_name_en: string;
        course_activity_short_name_th: string;
        course_activity_name_th: string;
        course_activity_name_en: string;
        course_activity_image: string;
        source_learning_video: string;
        test_time: number;
        test_question_number: number;
        minimum_score_percent: number;
        row_no: number;
        activity_uid: string;
        course_uid: string;
        questions: Question[];
    }

    

@Injectable({
  providedIn: 'root'
})
export class CourseActivityService extends BaseService {


  constructor(
      public http : HttpClient,
      private questionsSV: QuestionsService
  ) {
    super('/subject/course_activity',http);
  }

  put(data){
    return this.http.put(`https://rmutp.71dev.com/rmutp-mooc-api/api/subject/course_activity`,data)
  }

  getAllQuestion(courseActivity: string){

    let _currentQuestion
    return this.http.get<MoocCourseActivity>(`${environment.baseUrl}/api/subject/course_activity/${courseActivity}`).pipe(
        concatMap((questions) => {
        _currentQuestion = {...questions}
        let mapQuestion$ =  questions.questions.map(q => this.questionsSV.get(q.question_uid))
        return forkJoin(...mapQuestion$)
      }),
      map(question => {

          let mapQuestion = _currentQuestion.questions.map(q => {

            return {
              ...q,
              ...question.find(x => x.question_uid == q.question_uid)
            }
          })

          _currentQuestion.questions = [...mapQuestion]
          return  _currentQuestion
      })
    )
  }
}
