import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './../../base/base-service';
import { Injectable } from '@angular/core';
export interface MoocSubject {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  subject_uid: string;
  subject_code: string;
  subject_short_name_en: string;
  subject_short_name_th: string;
  subject_name_th: string;
  subject_name_en: string;
  subject_image: string;
  faculty_name: string;
  instructor_name: string;
  language_uid: string;
  skill_uid: string;
  is_certificate_reward: boolean;
  subject_description: string;
  subject_purpose: string;
  study_hour: number;
  course_number: number;
}

export interface TestDetailInformation {
  course_activity_uid: string;
  course_activity_name: string;
  is_complete?: any;
}

export interface TestInformation {
  test_uid: string;
  course_uid: string;
  course_name: string;
  is_complete?: any;
  test_detail_informations: TestDetailInformation[];
}

export interface TestSubjectInformation {
  subject_name: string;
  is_complete?: any;
  test_informations: TestInformation[];
}

export interface InformationSubject {
  test_subject_information: TestSubjectInformation;
  test?: any;
  course_activity?: any;
  questions?: any;
}

@Injectable({
  providedIn: 'root'
})
export class SubjectService extends BaseService {

  type
  constructor(public http : HttpClient) {
    super('/subject/subject',http);
  }
  Subjectuid
  Subjectdata

  getsize(p,s){
    return this.http.get(`https://rmutp.71dev.com/rmutp-mooc-api/api/subject/subject/`+p+'/'+s)
  }


  
}
