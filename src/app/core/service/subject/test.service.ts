import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'app/core/base/base-service';
import { InformationSubject, StartTest } from 'app/core/type';
import { Observable } from 'rxjs';

export interface StartCourse {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  test_uid: string;
  app_user_uid: string;
  course_uid: string;
  test_no: number;
  test_code: string;
  is_complete: boolean;
  test_details: TestDetail[];
}

export interface TestQuestion {
  test_question_uid: string;
  test_detail_uid: string;
  question_uid: string;
  selected_choice_uid?: any;
  test_question_answer: string;
  status_id?: any;
  created_by?: any;
  created_datetime?: any;
  updated_by?: any;
  updated_datetime?: any;
  search?: any;
  owner_agency_uid?: any;
}

export interface TestDetail {
  test_questions: TestQuestion[];
  test_detail_uid: string;
  test_uid: string;
  course_activity_uid: string;
  is_complete?: any;
  status_id?: any;
  created_by?: any;
  created_datetime?: any;
  updated_by?: any;
  updated_datetime?: any;
  search?: any;
  owner_agency_uid?: any;
}

export interface Test {
  test_details: TestDetail[];
  test_uid: string;
  test_subject_uid: string;
  course_uid: string;
  remark?: any;
  is_complete?: any;
  status_id?: any;
  created_by?: any;
  created_datetime?: any;
  updated_by?: any;
  updated_datetime?: any;
  search?: any;
  owner_agency_uid?: any;
}

export interface Enroll {
  tests: Test[];
  test_subject_uid: string;
  user_uid: string;
  subject_uid: string;
  test_subject_no: number;
  test_subject_code: string;
  remark?: any;
  is_complete?: any;
  status_id?: any;
  created_by?: any;
  created_datetime?: any;
  updated_by?: any;
  updated_datetime?: any;
  search?: any;
  owner_agency_uid?: any;
}

export interface TestDetailInformation {
  course_activity_uid: string;
  course_activity_name: string;
  is_complete?: any;
}



export interface Test {
  test_subject?: any;
  test_details: TestDetail[];
  test_uid: string;
  test_subject_uid: string;
  course_uid: string;
  remark?: any;
  is_complete?: any;
  status_id?: any;
  created_by?: any;
  created_datetime?: any;
  updated_by?: any;
  updated_datetime?: any;
  search?: any;
  owner_agency_uid?: any;
}

export interface CourseActivity {
  course?: any;
  questions?: any;
  course_activity_uid: string;
  course_activity_code?: any;
  course_activity_short_name_en?: any;
  course_activity_short_name_th?: any;
  course_activity_name_th: string;
  course_activity_name_en?: any;
  course_activity_image?: any;
  source_learning_video?: any;
  test_time: number;
  test_question_number: number;
  minimum_score_percent?: any;
  row_no: number;
  activity_uid: string;
  course_uid: string;
  status_id?: any;
  created_by?: any;
  created_datetime?: any;
  updated_by?: any;
  updated_datetime?: any;
  search?: any;
  owner_agency_uid?: any;
}










@Injectable({
  providedIn: 'root'
})
export class TestService extends BaseService {

  type
  constructor(
    public http : HttpClient
  ) {
    super('/test/test',http);
  }

  startTest(courseActivityUid: string,testSubjectUid: string): Observable<StartTest> {
    return this.http.get<StartTest>(`${this.fullUrl}/start_test/${courseActivityUid}/${testSubjectUid}`)
  }

  startEnroll(subjectUid: string,userUid: string): Observable<Enroll> {
    return this.http.get<Enroll>(`${this.fullUrl}/start_enroll/${subjectUid}/${userUid}`)
  }

  informationSubject(subjectUid: string,userUid: string): Observable<InformationSubject>{
    return this.http.get<InformationSubject>(`${this.fullUrl}/information_subject/${subjectUid}/${userUid}`)
  }

  submitTest(courseActivityUid: string,req): Observable<StartTest> {
    return this.http.post<StartTest>(`${this.fullUrl}/submit_test/${courseActivityUid}`,req)
  }

}