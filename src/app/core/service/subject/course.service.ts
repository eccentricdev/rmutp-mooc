import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'app/core/base/base-service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
export interface Choice {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  choice_uid: string;
  choice_name: string;
  question_uid: string;
}

export interface Question {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  question_uid: string;
  question_name: string;
  correct_answer: string;
  correct_choice_uid: string;
  course_activity_uid: string;
  choices: Choice[];
}

export interface CourseActivity {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  course_activity_uid: string;
  course_activity_code: string;
  course_activity_short_name_en: string;
  course_activity_short_name_th: string;
  course_activity_name_th: string;
  course_activity_name_en: string;
  course_activity_image: string;
  source_learning_video: string;
  test_time: number;
  test_question_number: number;
  minimum_score_percent: number;
  row_no: number;
  activity_uid: string;
  course_uid: string;
  questions: Question[];
}

export interface Cours {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  course_uid: string;
  subject_uid: string;
  course_code: string;
  course_short_name_en: string;
  course_short_name_th: string;
  course_name_th: string;
  course_name_en: string;
  course_image: string;
  course_activitys: CourseActivity[];
}

export interface MoocSubject {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  subject_uid: string;
  subject_code: string;
  subject_short_name_en: string;
  subject_short_name_th: string;
  subject_name_th: string;
  subject_name_en: string;
  subject_image: string;
  faculty_name: string;
  instructor_name: string;
  language_uid: string;
  skill_uid: string;
  is_certificate_reward: boolean;
  subject_description: string;
  subject_purpose: string;
  study_hour: number;
  course_number: number;
  courses: Cours[];
}


export interface Choice {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  choice_uid: string;
  choice_name: string;
  question_uid: string;
}

export interface Question {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  question_uid: string;
  question_name: string;
  correct_answer: string;
  correct_choice_uid: string;
  course_activity_uid: string;
  choices: Choice[];
}

export interface CourseActivity {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  course_activity_uid: string;
  course_activity_code: string;
  course_activity_short_name_en: string;
  course_activity_short_name_th: string;
  course_activity_name_th: string;
  course_activity_name_en: string;
  course_activity_image: string;
  source_learning_video: string;
  test_time: number;
  test_question_number: number;
  minimum_score_percent: number;
  row_no: number;
  activity_uid: string;
  course_uid: string;
  questions: Question[];
}

export interface MoocCourse {
  status_id: number;
  created_by: string;
  created_datetime: string;
  updated_by: string;
  updated_datetime: string;
  search: string;
  owner_agency_uid: string;
  course_uid: string;
  subject_uid: string;
  course_code: string;
  course_short_name_en: string;
  course_short_name_th: string;
  course_name_th: string;
  course_name_en: string;
  course_image: string;
  course_activitys: CourseActivity[];
}

@Injectable({
  providedIn: 'root'
})
export class CourseService extends BaseService {

  lesson
  quiz
  constructor(public http : HttpClient) {
    super('/subject/course',http);
  }

  put(data){
    return this.http.put(`https://rmutp.71dev.com/rmutp-mooc-api/api/subject/course`,data)
  }

  getCourse(courseUid: string): Observable<MoocCourse>{
    return this.http.get<MoocCourse>(`${environment.baseUrl}/api/subject/course/${courseUid}`)
  }

  getSubject(subjectUId: string): Observable<MoocSubject>{
    return this.http.get<MoocSubject>(`${environment.baseUrl}/api/subject/subject/${subjectUId}`)
  }
}


