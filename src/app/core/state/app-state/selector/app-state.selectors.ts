import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from 'app/state/reducers';
import { MoocState } from '../reducer/app-state.reducer';

const selectAppState = createFeatureSelector<AppState,MoocState>(
    'appState'
)

export const selectIsLogin = createSelector(
    selectAppState,
    state => state.authen.isLogin
)

export const selectinformationUser = createSelector(
    selectAppState,
    state => state.authen.user
)

export const selectSubject = createSelector(
    selectAppState,
    state => state.subject.selectSubject
)

export const selectTypeCurrentActivityCode = createSelector(
    selectAppState,
    state => state.currentTest?.selectedTest?.activity_code
)

export const selectTest = createSelector(
    selectAppState,
    state => state.currentTest.test
)

export const selectCurrentTest = createSelector(
    selectAppState,
    state => state.currentTest?.selectedTest
)

export const selectCurrentTestQuestion = createSelector(
    selectAppState,
    state => state.currentTest?.test?.questions
)

export const selectInformationSubject = createSelector(
    selectAppState,
    state => state.informationSubject?.informationSubject
)

// export const selectInformationSubjectCourseActivity = createSelector(
//     selectAppState,
//     state => state.informationSubject?.informationSubject?.course_activity
// )

export const selectInformationSubjectTest = createSelector(
    selectAppState,
    state => state.informationSubject?.informationSubject?.test_subject_information
)

export const selectIsStartTest = createSelector(
    selectAppState,
    state => state.currentTest.isStartTest
)

