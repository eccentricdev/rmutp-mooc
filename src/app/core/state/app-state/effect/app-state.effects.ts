import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppService } from 'app/core/service/app.service';
import { LoginService } from 'app/core/service/auth/login.service';
import { TestSubjectService } from 'app/core/service/subject/test-subject.service';
import { TestService } from 'app/core/service/subject/test.service';
import { ActivityType } from 'app/core/type';
import { AppState } from 'app/state/reducers';
import { of } from 'rxjs';
import { catchError, concatMap, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { AUTHEN, AUTHEN_ERROR, AUTHEN_SUCCESS, CONTINUES_TEST, CONTINUES_TEST_FAILURE, ENROLL, ENROLL_FAILURE, ENROLL_SUCCESS, LOAD_INFORMATION_SUBJECT, LOG_OUT, LOG_OUT_SUCESS,  SELECT_TEST,  SELECT_TEST_AND_START_TEST,   START_TEST_FAILURE, START_TEST_SUCCESS, SUBMIT_TEST, SUBMIT_TEST_SUCCESS, SUBMIT_TEST_TIME_OUT, SUBMIT_TSET_FAILURE } from '../action/app-state.actions';
import { selectCurrentTest, selectInformationSubject, selectinformationUser, selectTest } from '../selector/app-state.selectors';



@Injectable()
export class AppStateEffects {
  login$ = createEffect(() => {
    return  this.actions$.pipe(
      ofType(AUTHEN),
          switchMap(action => this.loginSV.add(action.payload).pipe(
                map(res => AUTHEN_SUCCESS({ user: res })),
                catchError(err => {
                  alert('ไม่สามารถเข้าสู่ระบบได้')
                  return of(AUTHEN_ERROR())
                })
            )),              
      )
  })

  continusTest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CONTINUES_TEST),
      withLatestFrom(this.store.select(selectinformationUser)),
      switchMap(([action,user]) => this.testSV.informationSubject(action.subjectUid,user.user_uid).pipe(
          concatMap((result) => [
                LOAD_INFORMATION_SUBJECT({ informationSubject: result}),
                SELECT_TEST_AND_START_TEST({ selectTest: result?.test_subject_information.test_informations[0].test_detail_informations[0] })
              ]),
            tap(() => this.router.navigate(['/app/main/enroll',action.subjectUid])),
            catchError(err => {
                    console.log(err)
                    return of(CONTINUES_TEST_FAILURE())
                })
          )),
          
    )
  })

  

  startEnroll$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ENROLL),
      withLatestFrom(this.store.select(selectinformationUser)),
      switchMap(([action,user]) => this.testSV.startEnroll(action.subjectUid,user.user_uid).pipe(
        map(res => ENROLL_SUCCESS({ enroll: res })),
        concatMap(() => this.testSV.informationSubject(action.subjectUid,user.user_uid)),
        withLatestFrom(
            this.store.select(selectCurrentTest),
            ),
          concatMap(([result,selectTest]) => {
                if(!result?.test_subject_information.test_informations.length){
                   throw new Error('ไม่เจอคำถาม ลอง enroll ใหม่')
                }
                if(!selectTest) {
                  return [
                    LOAD_INFORMATION_SUBJECT({ informationSubject: result}),
                    SELECT_TEST_AND_START_TEST({ selectTest: result?.test_subject_information.test_informations[0].test_detail_informations[0] })
                  ]
                }


                if(!result.test_subject?.tests?.some(t => t?.test_details.some(td => td?.course_activity_uid == selectTest?.course_activity_uid))){
                  return [
                    LOAD_INFORMATION_SUBJECT({ informationSubject: result}),
                    SELECT_TEST_AND_START_TEST({ selectTest: result.test_subject_information.test_informations[0].test_detail_informations[0] })
                  ]
                }

                return [
                  LOAD_INFORMATION_SUBJECT({ informationSubject: result})
                ]
              

              
        }),
        catchError(err => {
                  alert(err)
                  console.log(err)
                  return of(ENROLL_FAILURE())
            })    
      )),      
    )
  })


  selectTestAndStartTest$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SELECT_TEST_AND_START_TEST),
      withLatestFrom(this.store.select(selectInformationSubject)),
      switchMap(([action,informationSubject]) => this.testSV.startTest(action.selectTest.course_activity_uid,informationSubject.test_subject.test_subject_uid).pipe(
                concatMap((startTestResult) => {
                  return [
                    START_TEST_SUCCESS({ startTest: startTestResult }),
                    SELECT_TEST({ selectTest: action.selectTest })
                  ]
                })
           ))
      )
  })

  submitTest$ = createEffect(() => {
    let _test = null
    let resultInformationSubject = null
    return this.actions$.pipe(
      ofType(SUBMIT_TEST),
      withLatestFrom(this.store.select(selectTest),this.store.select(selectCurrentTest)),
      tap(([action, test,currentTest]) => _test = {...currentTest}),
      switchMap(([action, test,currentTest]) => this.testSV.submitTest(currentTest.course_activity_uid,test).pipe(
        map(res => SUBMIT_TEST_SUCCESS({ startTest: res })),
        withLatestFrom(
              this.store.select(selectInformationSubject),
              this.store.select(selectinformationUser)
            ),
        concatMap(([action,informationSubject,informationUser]) => this.testSV.informationSubject(informationSubject.test_subject.subject_uid,informationUser.user_uid)),
        tap((res) => {
                resultInformationSubject = {...res}
                if(_test.activity_code != ActivityType.LESSON){
                  this.appService.swaltAlert('ส่งคำตอบเรียบร้อยแล้ว','ส่งคำตอบเรียบร้อยแล้ว')
                }
              }),
        withLatestFrom(this.store.select(selectCurrentTest)),
        concatMap(([result,selectedTest]) => this.testSV.startTest(selectedTest.course_activity_uid,result.test_subject.test_subject_uid)),
        withLatestFrom(this.store.select(selectCurrentTest)),
        concatMap(([result,selectedTest])  => {
                  let selectCourse = result.test_subject_information.test_informations.find(test => test.course_uid == selectedTest.course_uid)
                  let _selectedTest = selectCourse.test_detail_informations.find((testDetal: any) =>  testDetal.course_activity_uid == selectedTest.course_activity_uid)
                  return [
                      SELECT_TEST({ selectTest: _selectedTest }),
                      LOAD_INFORMATION_SUBJECT({ informationSubject: resultInformationSubject })
                    ]
                }),
                catchError(err => {
                  console.log(err)
                  // alert('err happen')
                  return of(SUBMIT_TSET_FAILURE())
                })
              
      )),
    )
  })

  submitTestTimeOut$  = createEffect(() => {
    let _test = null
    let resultInformationSubject = null
    return this.actions$.pipe(
      ofType(SUBMIT_TEST_TIME_OUT),
      withLatestFrom(this.store.select(selectTest),this.store.select(selectCurrentTest)),
      tap(([action, test,currentTest]) => _test = {...currentTest}),
      switchMap(([action, test,currentTest]) => this.testSV.submitTest(currentTest.course_activity_uid,test).pipe(
        map(res => SUBMIT_TEST_SUCCESS({ startTest: res })),
        withLatestFrom(
              this.store.select(selectInformationSubject),
              this.store.select(selectinformationUser)
            ),
        concatMap(([action,informationSubject,informationUser]) => this.testSV.informationSubject(informationSubject.test_subject.subject_uid,informationUser.user_uid)),
        // map(result => LOAD_INFORMATION_SUBJECT({ informationSubject: result })),
        tap((res) => {
                resultInformationSubject = {...res}
                this.appService.swaltAlert('หมดเวลา !!','หมดเวลา !! กดตกลงเพื่อรับทราบ')          
            }),
        withLatestFrom(this.store.select(selectCurrentTest)),
        concatMap(([result,selectedTest]) => this.testSV.startTest(selectedTest.course_activity_uid,result.test_subject.test_subject_uid)),
        withLatestFrom(this.store.select(selectCurrentTest)),
        concatMap(([result,selectedTest])  => {
                  let selectCourse = result.test_subject_information.test_informations.find(test => test.course_uid == selectedTest.course_uid)
                  let _selectedTest = selectCourse.test_detail_informations.find((testDetal: any) =>  testDetal.course_activity_uid == selectedTest.course_activity_uid)
                  return [
                      SELECT_TEST({ selectTest: _selectedTest }),
                      LOAD_INFORMATION_SUBJECT({ informationSubject: resultInformationSubject })
                    ]
                }),
        catchError(err => {
              alert('err happen')
              return of(SUBMIT_TSET_FAILURE())
            })
      )),
    )
  })

  

  logout$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(LOG_OUT),
      map(() => LOG_OUT_SUCESS()),
      tap(() => this.router.navigate(['/app/main'],{ replaceUrl: true }))
    )
  },{ dispatch: false })


  constructor(
    private actions$: Actions,
    private loginSV: LoginService,
    private store: Store<AppState>,
    private testSV: TestService,
    private appService: AppService,
    private router: Router
  ) {}

}
