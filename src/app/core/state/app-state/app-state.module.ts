import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AppStateEffects } from './effect/app-state.effects';
import { appStateReducer } from './reducer/app-state.reducer';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature('appState',appStateReducer),
    EffectsModule.forFeature([
      AppStateEffects
    ])
  ],
  exports: [
    StoreModule,
    EffectsModule
  ]
})
export class AppStateModule { }
