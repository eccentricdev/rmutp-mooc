import { Action, createReducer, on } from "@ngrx/store";
import { UserDetails } from "app/core/service/auth/register.service";
import { MoocCourseActivity } from "app/core/service/subject/course-activity.service";
import { MoocSubject } from "app/core/service/subject/subject.service";
import { Enroll } from "app/core/service/subject/test.service";
import {  InformationSubject, StartTest, TestDetailInformation } from "app/core/type";
import {
  
  AUTHEN_ERROR,
  AUTHEN_SUCCESS,
  
  ENROLL_FAILURE,
  
  ENROLL_SUCCESS,
  
  LOAD_INFORMATION_SUBJECT,
  
  LOG_OUT_SUCESS,
  SELECT_SUBJECT,
  
  SELECT_TEST,
  
  SELECT_TEST_AND_START_TEST_SUCCESS,
  
  START_TEST_SUCCESS,
  SUBMIT_TEST_SUCCESS,
  UPDATE_STATE_INPUT,
  UPDATE_STATE_RADIO,
} from "../action/app-state.actions";

export const appStateFeatureKey = "appState";

export interface MoocState {
  authen: {
    isLogin: boolean;
    token: string
    user: UserDetails;
  };
  // selectCurrentAcitivity: {
  //     currentTest: StartTest      
  //     typeActivity: ActivityType
  // },
  subject: {
    selectSubject: MoocSubject
  },
  enroll: {
    isAlreadyEnroll:boolean,
    enroll: Enroll
  },
  informationSubject: {
    isLoad: boolean,
    informationSubject: InformationSubject
  },
  currentTest: {
    isStartTest: boolean
    test: StartTest
    selectedTest: TestDetailInformation
  }
}

export const initialState: MoocState = {  
  authen: {
    isLogin: false,
    token: null,
    user: null,
  },
  // selectCurrentAcitivity: {
  //   currentTest: null,
  //   typeActivity: ActivityType.TYPEINFORM
  // },
  subject: {
    selectSubject: null,
  },
  enroll: {
    isAlreadyEnroll: false,
    enroll: null
  },
  informationSubject: {
    isLoad: false,
    informationSubject: null
  },
  currentTest: {
    isStartTest: false,
    test: null,
    selectedTest: null
  }
  
  
};

export const reducer = createReducer(
  initialState,
  on(AUTHEN_SUCCESS, (state, action) => ({
    ...state,
    authen: {
      ...state.authen,
      isLogin: true,
      token: action.user.token,
      user: {
        email: action.user.userDetails.email,
        faculty_name: action.user.userDetails.faculty_name,
        full_name: action.user.userDetails.full_name,
        major_name: action.user.userDetails.major_name,
        user_uid: action.user.userDetails.user_uid,
      },
    },
  })),
  on(AUTHEN_ERROR, (state, ation) => ({
    ...state,
    authen: {
      ...state.authen,
      isLogin: false,
      token: null,
      user: null
    },
  })),

  on(SELECT_SUBJECT,(state, action) => ({
      ...state,
      subject: {
        ...state.subject,
        selectSubject: {
          ...state.subject.selectSubject,
          ...action.subject,
        }
      }
  })),
  on(ENROLL_SUCCESS,(state, action) => ({
      ...state,
      enroll: {
        ...state.enroll,
        isAlreadyEnroll: true,
        enroll: {
          ...action.enroll
        }
      }
  })),
  on(ENROLL_FAILURE,(state, action) => ({
    ...state,
    enroll: {
      ...state.enroll,
      isAlreadyEnroll: false,
      enroll: null
    }
  })),
  on(LOAD_INFORMATION_SUBJECT,(state,action) => ({
      ...state,
      informationSubject:{
        isLoad: true,
        informationSubject: {
          ...action.informationSubject
        }
      }
  })),
  on(START_TEST_SUCCESS,(state,action) => ({
      ...state,
      currentTest: {
        ...state.currentTest,
        isStartTest: true,
        test: action.startTest
      }
  })),
  on(SUBMIT_TEST_SUCCESS,(state, action) => ({
        ...state,
        currentTest: {
        ...state.currentTest,
        test: action.startTest
      }
  })),
  on(SELECT_TEST, (state,action) => ({
        ...state,
        currentTest: {
          ...state.currentTest,
          selectedTest: {
            ...state.currentTest.selectedTest,
            ...action.selectTest
          }
        }
  })),
  on(SELECT_TEST_AND_START_TEST_SUCCESS,(state, action) => ({
        ...state,
        currentTest: {
          ...state.currentTest,
          selectedTest: {
            ...state.currentTest.selectedTest,
            ...action.selectTest
          },
          test: {
            ...state.currentTest.test,
            ...action.resultStartTest
          }
        }
  })),
  on(UPDATE_STATE_RADIO,(state,action) => {
    let currentQuestionIndex = state.currentTest.test.questions.findIndex(q => q.question_uid == action.question.question_uid)    
    let questions = state.currentTest.test.questions.map((question, index) => currentQuestionIndex == index ? {...question, selected_choice_uid: action.choiceUid} :question)
    
    return {
      ...state,
      currentTest: {
        ...state.currentTest,
        test: {
          ...state.currentTest.test,
          questions: [
            ...questions
          ],
        }
      }
    }
  }),
  on(UPDATE_STATE_INPUT,(state,action) => {
    let currentQuestionIndex = state.currentTest.test.questions.findIndex(q => q.question_uid == action.question.question_uid)    
    let questions = state.currentTest.test.questions.map((question, index) => currentQuestionIndex == index ? {...question, test_question_answer: action.answer ,selected_choice_uid: null} :question)
    return {
      ...state,
      currentTest: {
        ...state.currentTest,
        test: {
          ...state.currentTest.test,
          questions: [
            ...questions
          ]
        }
      }
    }
  }),  
  on(LOG_OUT_SUCESS,(state,action) => ({
    ...state,
    authen: {
      ...state.authen,
      isLogin: false,
      token: null,
      user: null,
    },

    subject: {
      ...state.subject,
      selectSubject: null,
    },
    enroll: {
      isAlreadyEnroll: false,
      enroll: null
    },
    informationSubject: {
      isLoad: false,
      informationSubject: null
    },
    currentTest: {
      isStartTest: false,
      test: null,
      selectedTest: null
    }
  }))
);


export function appStateReducer(action,state){
  return reducer(action,state)
}
