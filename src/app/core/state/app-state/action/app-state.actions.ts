import { createAction, props } from '@ngrx/store';
import { User } from 'app/core/service/auth/register.service';

import { MoocSubject } from 'app/core/service/subject/course.service';
import { Enroll,  StartCourse } from 'app/core/service/subject/test.service';
import {  InformationSubject, StartTest, TestDetailInformation } from 'app/core/type';

export const AUTHEN = createAction(
  '[authen] start authen', 
  props<{ payload: any }>()
)

export const AUTHEN_SUCCESS = createAction(
  '[authen] authne success', 
  props<{ user: User }>()
)

export const AUTHEN_ERROR = createAction(
  '[authen] authen error',  
)

export const SELECT_TEST = createAction(
  '[test] select test', 
  props<{ selectTest: TestDetailInformation }>()
)

export const SELECT_TEST_AND_START_TEST = createAction(
  '[test] select test and start call test', 
  props<{ selectTest: TestDetailInformation}>()
)

export const SELECT_TEST_AND_START_TEST_SUCCESS = createAction(
  '[test] select test and start call test success', 
  props<{ selectTest: TestDetailInformation, resultStartTest: StartTest }>()
)



export const START_EXAM = createAction(
  '[examination] start ex',
  props<{ isStart: boolean }>()
)

export const SELECT_SUBJECT = createAction(
  '[subject] select subject',
  props<{ subject: MoocSubject}>()
)

export const LOG_OUT = createAction(
  '[auth] start logout'
)

export const LOG_OUT_SUCESS = createAction(
  '[auth] start logout'
)



export const ENROLL = createAction(
  '[course] start enroll',
  props<{ subjectUid: string }>()
)

export const ENROLL_SUCCESS = createAction(
  '[course] start enroll success',
  props<{ enroll: Enroll }>()
)

export const  ENROLL_FAILURE = createAction(
  '[course] start enroll error',
)

export const LOAD_INFORMATION_SUBJECT = createAction(
  '[inormationSubject] load information subject',
      props<{ informationSubject: InformationSubject }>()
)

export const CONTINUES_TEST = createAction(
  '[test] continues test', 
  props<{ subjectUid: string }>()
)

export const CONTINUES_TEST_SUCCESS = createAction(
  '[test] continues test success',
)

export const CONTINUES_TEST_FAILURE = createAction(
  '[test] continues test fail',
)


export const START_TEST_SUCCESS = createAction(
  '[start test] start call test success',
  props<{ startTest: StartTest }>()
)

export const START_TEST_FAILURE = createAction(
  '[start test] start call test failure',  
)

export const SUBMIT_TEST_TIME_OUT = createAction(
  '[submit test] start submit test time out',  
 
)

export const SUBMIT_TEST = createAction(
  '[submit test] start submit test',  
  
)

export const SUBMIT_TEST_SUCCESS = createAction(
  '[submit test] start submit test success',  
  props<{ startTest: StartTest }>()
)

export const SUBMIT_TSET_FAILURE = createAction(
  '[submit test] start submit test error',  
)

export const UPDATE_STATE_RADIO = createAction(
  '[update state] update state radio',
  props<{ choiceUid: string, question: any}>()
)

export const UPDATE_STATE_INPUT = createAction(
  '[update state] update state input',
  props<{ answer: string, question: any}>()
)
