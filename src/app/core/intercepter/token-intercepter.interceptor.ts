
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { NgProgress } from 'ngx-progressbar';
import { finalize } from 'rxjs/operators';
import { OdicOpenService } from '../service/odic/odic-open.service';
import { AppTokenService } from '../service/token/app-token.service';

@Injectable()
export class TokenIntercepterInterceptor implements HttpInterceptor {
  private token :string = ''
  user
  constructor(
    private OdicOpenService: OdicOpenService,
    private progress: NgProgress,
    private tokenSV:AppTokenService
  ) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // console.log(this.tokenSV.tokenUser)
    const customReq = request.clone({
      setHeaders:{
          // Authorization:  this.tokenSV.tokenUser ? "Bearer "+this.tokenSV.tokenUser?.token : this.OdicOpenService.getAuthorizationHeaderValue() 
          
      }
    })

    // console.log(customReq)
    this.progress.ref('home-progress').start()
    return next.handle(customReq).pipe(
      finalize(() => this.progress.ref('home-progress').complete())
    );

  
    
  }
}
