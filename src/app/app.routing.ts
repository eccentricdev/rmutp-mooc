import { Route } from '@angular/router';

import { LayoutComponent } from 'app/layout/layout.component';
import { InitialDataResolver } from 'app/app.resolvers';
import { AuthGuard } from './core/guards/auth.guard';

export const appRoutes: Route[] = [

    // Redirect empty path to '/example'
    {path: '', pathMatch : 'full', redirectTo: '/app/main'},

    // Redirect signed in user to the '/example'
    //
    // After the user signs in, the sign in page will redirect the user to the 'signed-in-redirect'
    // path. Below is another redirection for that path to redirect the user to the desired
    // location. This is a small convenience to keep all main routes together here on this file.
    {path: 'signed-in-redirect', pathMatch : 'full', redirectTo: '/app/main'},
    {
        path: 'auth-callback',
        loadChildren: () => import('./feature/authen/authen.module')
          .then(res => res.AuthenModule)
      },
    // Admin routes
    {
        path       : 'app',
        // canActivate: [AuthGuard],
        // canActivateChild: [AuthGuard],
        component  : LayoutComponent,
        resolve    : {
            initialData: InitialDataResolver,
        },
        children   : [
            {
                path: 'main', 
                loadChildren: () => import('app/feature/main/main.module')
                .then(m => m.MainModule)
            },
            {
                path: 'subject', 
                loadChildren: () => import('./feature/subjects/subject/subject.module')
                .then(m => m.SubjectModule)
            },
            {
                path: 'structure-subject', 
                loadChildren: () => import('./feature/subjects/structure/structure.module')
                .then(m => m.StructureModule)
            },
            {
                path:'profile',
                loadChildren: () => import('./feature/profile/profile.module')
                    .then(m => m.ProfileModule)
            }
        ]
    }
];
