/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id      : 'การจัดการบทเรียน',
        title   : 'การจัดการบทเรียน',
        subtitle: '',
        type    : 'collapsable',
        // icon    : 'heroicons_outline:home',
        children: [
            {
                id   : 'รายละเอียดวิชา',
                title: 'รายละเอียดวิชา',
                type : 'basic',
                // icon : 'heroicons_outline:clipboard-check',
                link : '/app/subject',
                // exactMatch: true
            }
            ,
            {
                id   : 'โครงสร้างวิชา',
                title: 'โครงสร้างวิชา',
                type : 'basic',
                // icon : 'heroicons_outline:clipboard-check',
                link : '/app/structure-subject',
                // exactMatch: true
            }
        ]
    },
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id   : 'example',
        title: 'Example',
        type : 'basic',
        icon : 'heroicons_outline:chart-pie',
        link : '/example'
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id   : 'example',
        title: 'Example',
        type : 'basic',
        icon : 'heroicons_outline:chart-pie',
        link : '/example'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id   : 'example',
        title: 'Example',
        type : 'basic',
        icon : 'heroicons_outline:chart-pie',
        link : '/example'
    }
];
