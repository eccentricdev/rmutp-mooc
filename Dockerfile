FROM node:12.19.0-buster AS compile-image

WORKDIR /opt/ng
COPY package.json /opt/ng/package.json
RUN npm install
RUN npm install -g @angular/cli
ENV PATH="./node_modules/.bin:$PATH" 

COPY . ./
RUN node --max_old_space_size=7168 node_modules/@angular/cli/bin/ng build --configuration production --base-href /rmutp-mooc/ --deploy-url /rmutp-mooc/

FROM nginx
COPY nginx.conf /etc/nginx/conf.d/default.conf 
COPY --from=compile-image /opt/ng/dist/rmutp-mooc /usr/share/nginx/html